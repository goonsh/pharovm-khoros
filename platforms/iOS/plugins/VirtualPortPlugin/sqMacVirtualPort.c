/*
 *  sqMacVirtualPort.c
 *  VirtualPortPlugin
 *
 *  Created by NISHIHARA Satoshi on 12/02/03.
 *  Copyright 2012 goonsh@gmail.com. All rights reserved.
 *
 */

/* 
 * by Gary P. Scavone, author of RtMidi: realtime MIDI i/o C++ classes.
 *
 * http://www.music.mcgill.ca/~gary/rtmidi/index.html#virtual
 *
 *
 * The Linux ALSA and Macintosh CoreMIDI APIs allow for the
 * establishment of virtual input and output MIDI ports to which other
 * software clients can connect. 
 *
 */
/*
 * Any messages sent by PortMidiPlugin will also be transmitted through 
 * an open virtual output port.
 * If a virtual input port is open, the callback function will be invoked 
 * when messages arrive via that port. And signal to semaphore, by the 
 * index of callback semaphore.
 * No notification is provided for the establishment of a client connection 
 * via a virtual port.
 */

#include "sq.h"

#include "portmidi_filters.h"
#include "sqMacVirtualPort.h"
#include "sqMacDebug.h"

#ifdef __LP64__
#define NULL_REF 0
#else
#define NULL_REF NULL
#endif

#define MINIMUM_SYSTEM_VERSION 0x00001030
#define MIDI_STATUS_MASK 0x80
#define MIDI_SYSEX_END 0xF7
#define MIDI_SYSEX_START 0xF0
#define MIDI_REALTIME_MASK 0xF8


static Boolean vp_is_inited = false;
static void (*externalFunc)(void) = 0;
static char *externalPluginName = "PortMidiPlugin";
static Boolean useOwnClient = false;
static MIDIClientRef ownClient = NULL_REF;

enum {
	kVPNoPortMidiPlugin = -37458,	/* there is no PortMidiPlugin. */
	kVPIndexOutOfRange,				/* given index is out of renge of ports. */
	kVPDataTooShort,				/* 2 bytes data, first is data byte, but runningStatus is not set. */
	kVPMIDIPacketListAddError,		/* there was not room in the packet for the event. */
	kVPPortMidiPluginDoesNotSupportVirtualPort, /* PortMidiPlugin is found. but it not support vitrual port. */
	kVPPortNotCreated,				/* port is not opened. */
	kVPBadDirection,				/* port direction mismatch. */
};
		
// virtual port endpoints
#define MAX_VIRTUAL_OUTPUT 2
#define MAX_VIRTUAL_INPUT 2
#define MAX_VIRTUAL_PORTS (MAX_VIRTUAL_INPUT + MAX_VIRTUAL_OUTPUT)

#define VIRTUAL_OUTPUT 0					// I/O direction flags
#define VIRTUAL_INPUT 1
#define VIRTUAL_UNDEFINED -1
#define	RECEIVE_ALL_CHANNEL	0xFFFF

typedef struct VirtualPortStruct {
	MIDIEndpointRef endpoint;
	unsigned char runningStatus;
	/* flags that filter incoming message classes */
	int32_t filters;
	/* filter incoming messages based on channel */
	int32_t channel_mask;
	/* currently used in input only */
	UInt64 latest_timeStamp;
	/* which direction (in/out) */
	int direction;
} VirtualPortStruct;

static VirtualPortStruct v_endpoints[MAX_VIRTUAL_PORTS];

#define isOpenedNoChekIndex(index) (v_endpoints[index].endpoint != NULL_REF)

/*
 static _Bool isOpenedNoChekIndex(int index) {
 return v_endpoints[index].endpoint != NULL_REF;
 }
 */

/* callback */
#define CALLBACKMIDIMESSAGE_BYTE_SIZE 256
struct CallbackMidiMessage {
	Byte bytes[CALLBACKMIDIMESSAGE_BYTE_SIZE];
	UInt16 length;
	UInt16 destinationIndex;
	UInt64 timeStamp;
} CallbackMidiMessage;
static struct CallbackMidiMessage callbackMidiMessage;

static int callbackSemaphoreIndex = 0;
static int notifySemaphoreIndex = 0;
static char notifyMessage[1024];

static int currentPlatformsProcessorType = 0;

extern struct VirtualMachine *interpreterProxy;

/* externs */
extern const char *getDefaultVirtualInputPortName(void);
extern const char *getDefaultVirtualOutputPortName(void);
extern unsigned int composeUnsignedLongLongOopLowHigh(const unsigned long lowWord, const unsigned long highWord);
extern int decomposeUnsignedLongLongOopLowHigh(sqInt oop, unsigned int *lowWordPtr, unsigned int *highWordPtr);


/* callback private functions */

static UInt32 UInt64LowWord(UInt64 value) {
	if (currentPlatformsProcessorType != gestaltIntel) {
		return ((int *) &value)[1];
	}
	return ((int *) &value)[0];
}

static UInt32 UInt64HighWord(UInt64 value) {
	if (currentPlatformsProcessorType != gestaltIntel) {
		return ((int *) &value)[0];
	}
	return ((int *) &value)[1];
}

static void initializeCallbackMidiMessage(void) {
	callbackMidiMessage.length = 0;
	callbackMidiMessage.destinationIndex = 0;
	callbackMidiMessage.timeStamp = 0;
	memset(callbackMidiMessage.bytes, 0, sizeof(callbackMidiMessage.bytes));
}

static inline _Bool is_real_time(unsigned char status) {
	return ((((status) & 0xFF) & MIDI_REALTIME_MASK) == MIDI_REALTIME_MASK);
}

/* realtime_filtered returns non-zero if the filter will kill the current message.
 Note that only realtime messages are checked here.
 */
static inline _Bool realtime_filtered(unsigned char status, int32_t filters) {
	return ((((status) & 0xF0) == 0xF0) && ((1 << ((status) & 0xF)) & (filters)));
}

/* status_filtered returns non-zero if a filter will kill the current message, based on status.
 Note that sysex and real time are not checked.  It is up to the subsystem (winmm, core midi, alsa)
 to filter sysex, as it is handled more easily and efficiently at that level.
 Realtime message are filtered in pm_realtime_filtered.
 */
static inline _Bool status_filtered(unsigned char status, int32_t filters) {
	return ((1 << (16 + ((status) >> 4))) & (filters));
}

static inline int mask_channel(int channel) {
	return (1 << (channel));
}

/* channel_filtered returns non-zero if the channel mask is blocking the current channel */
static inline _Bool channel_filtered(unsigned char status, int32_t mask) {
	return ((((status) & 0xF0) != 0xF0) && (!(mask_channel((status) & 0x0F) & (mask))));
}

static void setAndSignalCallbackMidiMessage(const MIDIPacket *packet, int destinationIndex) {
	if (packet->length <= 0) {
		initializeCallbackMidiMessage();
		return;
	}
	VirtualPortStruct endpoint = v_endpoints[destinationIndex];
	unsigned char status = ((packet->data[0]) & 0xFF);
	if (status_filtered(status, endpoint.filters) 
		|| ((is_real_time(status) || realtime_filtered(status, endpoint.filters))) 
		|| channel_filtered(status, endpoint.channel_mask)) {
		return;
	}
	if (callbackSemaphoreIndex > 0) {
		
#ifndef _sqMacVirtualPort_debug_
		interpreterProxy->signalSemaphoreWithIndex(callbackSemaphoreIndex);
#endif //_sqMacVirtualPort_debug_
		
		callbackMidiMessage.length = packet->length;
		callbackMidiMessage.destinationIndex = destinationIndex;
		callbackMidiMessage.timeStamp = packet->timeStamp;
		memcpy(callbackMidiMessage.bytes, packet->data, packet->length);
#ifdef _sqMacVirtualPort_debug_
		debugWrite("timeStamp =%llu getMIDIClock=%lu\n", 
				   packet->timeStamp, 
				   getMIDIClock());
		debugWrite("low=%u higt=%u\n", UInt64LowWord(callbackMidiMessage.timeStamp), UInt64HighWord(callbackMidiMessage.timeStamp));
		interpreterProxy->signalSemaphoreWithIndex(callbackSemaphoreIndex);
#endif //_sqMacVirtualPort_debug_
	}
}

/* called when MIDI packets are received from virtual input */
static void readProcForDestination(const MIDIPacketList *list, void *readProcRefCon, void *srcConnRefCon) {
	int i;
	UInt16 destinationIndex = (unsigned int) readProcRefCon;
	
	const MIDIPacket *packet = &list->packet[0];
    for (i = 0; i < list->numPackets; i++) {
		if (packet->length == 0) continue;
#ifdef _sqMacVirtualPort_debug_
		int j;
		for (j = 0; j < packet->length; j++) {
			debugWrite("packet->data[%d/%d]=%2.2x\n", j, packet->length, packet->data[j]);
		}
#endif //_sqMacVirtualPort_debug_
		setAndSignalCallbackMidiMessage(packet, destinationIndex);
		v_endpoints[destinationIndex].latest_timeStamp = packet->timeStamp;;
		packet = MIDIPacketNext(packet);	
	}
}

/*
 *	https://developer.apple.com/library/mac/#qa/qa1374/_index.html
 * it run in Panther (10.3)
 * other MIDI function are AVAILABLE_MAC_OS_X_VERSION_10_0_AND_LATER;
 */

static Boolean checkSystemVersion(long minimumSystemVersion) {
	OSStatus err;
	long response;
	
	err = Gestalt(gestaltSystemVersion, &response);
	return ((err == noErr) && (response >= minimumSystemVersion));
}

static void	setCurrentPlatformsProcessorType(void) {
	OSStatus err;
	long response;

	err = Gestalt(gestaltSysArchitecture, &response);
	if (err != noErr) {
		return;
	}
	switch (response) {
		case gestaltIntel:
			currentPlatformsProcessorType = gestaltIntel;
			break;
		case gestaltPowerPC:
			currentPlatformsProcessorType = gestaltPowerPC;
			break;
		//case gestalt68k:
		//	currentPlatformsProcessorType = gestalt68k;
		//	break;
		default:
			break;
	}
	return;
}

static Boolean isUniqueIDInUse(SInt32 uniqueID) {
    int count;
    int index;
    MIDIEndpointRef endpoint;
    SInt32 usedID;
    
    count = MIDIGetNumberOfSources();
    for (index = 0; index < count; index++) {
        endpoint = MIDIGetSource(index);
        MIDIObjectGetIntegerProperty(endpoint, kMIDIPropertyUniqueID, &usedID);
        if (usedID == uniqueID) return true;
    }
    
    count = MIDIGetNumberOfDestinations();
    for (index = 0; index < count; index++) {
        endpoint = MIDIGetDestination(index);
        MIDIObjectGetIntegerProperty(endpoint, kMIDIPropertyUniqueID, &usedID);
        if (usedID == uniqueID) return true;
    }
    return false;
}

static SInt32 allocateUniqueID(void) {
    SInt32 uniqueID;
    static SInt32 sequence = 0;
    
    do {
        uniqueID = time(NULL) + sequence++;
    } while (isUniqueIDInUse(uniqueID));
    return uniqueID;
}

static void setUniqueIDfor(MIDIEndpointRef endpoint) {
	OSStatus result;
	SInt32 newUniqueID;
	result = MIDIObjectGetIntegerProperty(endpoint, kMIDIPropertyUniqueID, &newUniqueID);
	if (result == kMIDIUnknownProperty) {
		newUniqueID = allocateUniqueID();
		MIDIObjectSetIntegerProperty(endpoint, kMIDIPropertyUniqueID, newUniqueID);
	}
}

/*
 https://developer.apple.com/library/mac/#qa/qa1374/_index.html
 Technical Q&A QA1374
 Obtaining the name of an external MIDI Device from a MIDI Endpoint
 
 Q: In Panther, I am attempting to retrieve the name of an external
 MIDI device from a MIDI Object, but I get the name of the MIDI port
 instead. This seemed to work fine in Jaguar. How do I get the name
 of an external MIDI Device from a MIDI Endpoint in Panther
 */

/*
 * Obtain the name of an endpoint, following connections.
 * The result should be released by the caller.
 //////////////////////////////////////
 * Obtain the name of an endpoint without regard for whether it has connections.
 * The result should be released by the caller.
 */
static CFStringRef EndpointName(MIDIEndpointRef endpoint, bool isExternal)
{
	CFMutableStringRef result = CFStringCreateMutable(NULL, 0);
	CFStringRef str;
	
	/* begin with the endpoint's name */
	str = NULL;
	MIDIObjectGetStringProperty(endpoint, kMIDIPropertyName, &str);
	if (str != NULL) {
		CFStringAppend(result, str);
		CFRelease(str);
	}
	
	MIDIEntityRef entity = NULL;
	MIDIEndpointGetEntity(endpoint, &entity);
	if (entity == NULL)
	/* probably virtual */
		return result;
	
	if (CFStringGetLength(result) == 0) {
		/* endpoint name has zero length -- try the entity */
		str = NULL;
		MIDIObjectGetStringProperty(entity, kMIDIPropertyName, &str);
		if (str != NULL) {
			CFStringAppend(result, str);
			CFRelease(str);
		}
	}
	/* now consider the device's name */
	MIDIDeviceRef device = NULL;
	MIDIEntityGetDevice(entity, &device);
	if (device == NULL)
		return result;
	
	str = NULL;
	MIDIObjectGetStringProperty(device, kMIDIPropertyName, &str);
	if (str != NULL) {
		/*
		 * if an external device has only one entity, throw away 
		 * the endpoint name and just use the device name
		 */
		if (isExternal && MIDIDeviceGetNumberOfEntities(device) < 2) {
			CFRelease(result);
			return str;
		} else {
			/*
			 * does the entity name already start with the device name? 
			 * (some drivers do this though they shouldn't)
			 * if so, do not prepend
			 */
			if (CFStringCompareWithOptions(str /* device name */, 
										   result /* endpoint name */, 
										   CFRangeMake(0, CFStringGetLength(str)), 0) != kCFCompareEqualTo) {
				/* prepend the device name to the entity name */
				if (CFStringGetLength(result) > 0)
					CFStringInsert(result, 0, CFSTR(" "));
				CFStringInsert(result, 0, str);
			}
			CFRelease(str);
		}
	}
	return result;
}

static CFStringRef ConnectedEndpointName(MIDIEndpointRef endpoint)
{
	CFMutableStringRef result = CFStringCreateMutable(NULL, 0);
	CFStringRef str;
	OSStatus err;
	int i;
	
	/* Does the endpoint have connections? */
	CFDataRef connections = NULL;
	int nConnected = 0;
	bool anyStrings = false;
	err = MIDIObjectGetDataProperty(endpoint, kMIDIPropertyConnectionUniqueID, &connections);
	if (connections != NULL) {
		/*
		 * It has connections, follow them
		 * Concatenate the names of all connected devices
		 */
		nConnected = CFDataGetLength(connections) / sizeof(MIDIUniqueID);
		if (nConnected) {
			/*const SInt32 *pid = reinterpret_cast<const SInt32 *>(CFDataGetBytePtr(connections));*/
			const SInt32 *pid = (const SInt32 *)(CFDataGetBytePtr(connections));
			for (i = 0; i < nConnected; ++i, ++pid) {
				MIDIUniqueID id = EndianS32_BtoN(*pid);
				MIDIObjectRef connObject;
				MIDIObjectType connObjectType;
				err = MIDIObjectFindByUniqueID(id, &connObject, &connObjectType);
				if (err == noErr) {
					if (connObjectType == kMIDIObjectType_ExternalSource  || 
						connObjectType == kMIDIObjectType_ExternalDestination) {
						/* Connected to an external device's endpoint (10.3 and later). */
						/*str = EndpointName(static_cast<MIDIEndpointRef>(connObject), true);*/
						str = EndpointName((MIDIEndpointRef)(connObject), true);
					} else {
						/* Connected to an external device (10.2) (or something else, catch-all) */
						str = NULL;
						MIDIObjectGetStringProperty(connObject, kMIDIPropertyName, &str);
					}
					if (str != NULL) {
						if (anyStrings)
							CFStringAppend(result, CFSTR(", "));
						else anyStrings = true;
						CFStringAppend(result, str);
						CFRelease(str);
					}
				}
			}
		}
		CFRelease(connections);
	}
	if (anyStrings)
		return result;
	
	/* Here, either the endpoint had no connections, or we failed to obtain names for any of them. */
	return EndpointName(endpoint, false);
}

static char *getEndpointName(MIDIEndpointRef endpoint) {
	CFStringRef endpointNameRef;
	char *endpointName;
	
	endpointNameRef = ConnectedEndpointName(endpoint);
	endpointName = (char *) malloc(CFStringGetLength(endpointNameRef) + 1);
	CFStringGetCString(endpointNameRef, endpointName, CFStringGetLength(endpointNameRef) + 1, CFStringGetSystemEncoding());
	CFRelease(endpointNameRef);
	return endpointName;
}

/* initialize array for destinations and sources */
static void initializeInternalPorts(void) {
	int i;
	
	for (i = 0; i < MAX_VIRTUAL_PORTS; i++) {
		v_endpoints[i].endpoint = NULL_REF;
		v_endpoints[i].runningStatus = 0;
		v_endpoints[i].latest_timeStamp = 0LLU;
		v_endpoints[i].filters = PM_FILT_ACTIVE;
		v_endpoints[i].channel_mask = RECEIVE_ALL_CHANNEL;
		v_endpoints[i].direction = VIRTUAL_UNDEFINED;
	}
}

static void *lookupFunctionAddressfrom(char *functionName, char *pluginName) {
	void (*funcAddress)(void) = 0;
	
	funcAddress = interpreterProxy->ioLoadFunctionFrom(functionName, pluginName);
	if (funcAddress == 0) {
		funcAddress = interpreterProxy->ioLoadFunctionFrom(functionName, "");
	}
	return funcAddress;
}

static MIDIClientRef lookupMIDIClientRef(void) {
	MIDIClientRef client = NULL_REF;
	
	if (externalFunc != 0) {
		client = ((MIDIClientRef (*) (void)) externalFunc)();
#ifdef _sqMacVirtualPort_debug_
		debugWrite("sqMacVirtualPort.c>>*getMIDIClientRef client=%ld\n", client);
#endif //_sqMacVirtualPort_debug_
	}
	return client;
}

static char *createMIDINotificationMessage(MIDIObjectRef objectRef, const char *message) {
	char *str;
	CFStringRef strRef;

	sprintf(notifyMessage, "%s", message);
	if (objectRef != NULL) {
		MIDIObjectGetStringProperty(objectRef, kMIDIPropertyName, &strRef);
		if (strRef != NULL) {
			str = (char *) malloc(CFStringGetLength(strRef) + 1);
			CFStringGetCString(strRef, str, CFStringGetLength(strRef) + 1, CFStringGetSystemEncoding());
			CFRelease(strRef);
			sprintf(notifyMessage, "[%s] %s", str, message);
		}
	}
	return notifyMessage;
}

static void midiNotifyProc(const MIDINotification *message, void *refcon) {
	if (externalFunc != 0) {
		void (*func)(void) = 0;
		func = lookupFunctionAddressfrom("receivedMIDINotification", externalPluginName);
		if (func != 0) {
			((void (*) (const MIDINotification *message)) func)(message);
		}
	}
	MIDIObjectRef objectRef;
	switch (message->messageID) {
		case kMIDIMsgSetupChanged:
			// compatibility
			//Some aspect of the current MIDISetup has changed.  No data.  
			//Should ignore this message if messages 2-6 are handled.
			//createMIDINotificationMessage(NULL, "SetupChanged : Some aspect of the current MIDISetup has changed.");
			return;
			break;
		case kMIDIMsgObjectAdded:
			createMIDINotificationMessage(((MIDIObjectAddRemoveNotification *) message)->child, "Object Added: A MIDI device, entity or endpoint was added.");
			break;
		case kMIDIMsgObjectRemoved:
			//already removed
		{
			MIDIEndpointRef endpoint_ref = (MIDIEndpointRef) (((MIDIObjectAddRemoveNotification *) message)->child);
			objectRef = endpoint_ref;
		}
			createMIDINotificationMessage(objectRef, "Object Removed: A MIDI device, entity or endpoint was removed.");
			break;
		case kMIDIMsgPropertyChanged:
			createMIDINotificationMessage(((MIDIObjectPropertyChangeNotification *) message)->object, "Property Changed: An object's property was changed.");
			break;
		case kMIDIMsgThruConnectionsChanged:
			//MIDINotification
			createMIDINotificationMessage(NULL, "Thru Connections Changed: A persistent MIDI Thru connection was created or destroyed.");
			break;
		case kMIDIMsgSerialPortOwnerChanged:
			//MIDINotification
			createMIDINotificationMessage(NULL, "SerialPort Owner Changed: A persistent MIDI Thru connection was created or destroyed.");
			break;
		case kMIDIMsgIOError:
			createMIDINotificationMessage(((MIDIIOErrorNotification *) message)->driverDevice, "IO Error: A MIDI driver I/O error occurred.");
			break;
		default:
			break;
	}
	if (notifySemaphoreIndex > 0) {
		interpreterProxy->signalSemaphoreWithIndex(notifySemaphoreIndex);
	}
}

static MIDIClientRef createMIDIClientRef(void) {
	OSStatus result = noErr;

    /* Initialize the client handle */
	CFStringRef name = CFStringCreateWithCString(NULL, "Squeak", kCFStringEncodingASCII);
	MIDINotifyProc notifyProc = midiNotifyProc;
	void *notifyRefCon = NULL;
    result = MIDIClientCreate(name, notifyProc, notifyRefCon, &ownClient);
    if (result != noErr) {
        goto error_return;
	}
#ifdef _sqMacVirtualPort_debug_
	debugWrite("ownClient created [%ld]\n", ownClient);
#endif //_sqMacVirtualPort_debug_
	return ownClient;

error_return:
	MIDIClientDispose(ownClient);
	ownClient = NULL_REF;
	return ownClient;
}

static OSStatus externalPluginStatus(void (*func)(void)) {
	if (func == 0) {
		if (lookupFunctionAddressfrom("getModuleName", externalPluginName) == 0) {
			/* there is no PortMidiPlugin. */
			return kVPNoPortMidiPlugin;
		} 
		/* PortMidiPlugin is found. but it not support vitrual port. */
		return kVPPortMidiPluginDoesNotSupportVirtualPort;
	}
	return noErr;
}

static Boolean decideUseOwnClient(void (*func)(void)) {
	if (externalPluginStatus(func) == noErr) {
		useOwnClient = true;
	}
	return useOwnClient;
}

static MIDIClientRef lookupOrCreateMIDIClientRef(void) {
	MIDIClientRef client = lookupMIDIClientRef();
	if (client == NULL_REF) {
		if (useOwnClient) {
			client = createMIDIClientRef();
		}
	}
	return client;
}

static void setExternalFunctionAddress(void) {
	externalFunc = lookupFunctionAddressfrom("getMIDIClientRef", externalPluginName);
	decideUseOwnClient(externalFunc);
}

static int initializeExternalPluginIfNecessary(void) {
	void (*funcAddress)(void) = 0;
	int result = -1;
	
	funcAddress = lookupFunctionAddressfrom("pm_initialize", externalPluginName);
	if (funcAddress != 0) {
		result = ((int (*) (void)) funcAddress)();
	} else {
		result = kVPNoPortMidiPlugin;
	}
	return result;
}

static OSStatus openVirtualInputPort(MIDIClientRef client, MIDIEndpointRef *endpoint, int index) {
	OSStatus result;
	char indexedPortName[256];
	extern void readProcForDestination(const MIDIPacketList *list, void *readProcRefCon, void *srcConnRefCon);
	
	/* Create a virtual MIDI input destination. */
	sprintf(indexedPortName, "%s %d", getDefaultVirtualInputPortName(), index + 1);
	void *refCon = (void *) index;
	MIDIReadProc readProc = readProcForDestination;
	result = MIDIDestinationCreate(client,
								   CFStringCreateWithCString(NULL, indexedPortName, kCFStringEncodingASCII),
								   readProc, refCon, endpoint);
	if (result != noErr) {
		MIDIEndpointDispose(*endpoint);
	}
	return result;
}

static OSStatus openVirtualOutputPort(MIDIClientRef client, MIDIEndpointRef *endpoint, int index) {
	OSStatus result;
	char indexedPortName[256];
	
	/* Create a virtual MIDI output source. */
	sprintf(indexedPortName, "%s %d", getDefaultVirtualOutputPortName(), index - MAX_VIRTUAL_INPUT + 1);
	result = MIDISourceCreate(client,
							  CFStringCreateWithCString(NULL, indexedPortName, kCFStringEncodingASCII),
							  endpoint);
	if (result != noErr) {
		MIDIEndpointDispose(*endpoint);
	}
	return result;
}

static int getFilter(unsigned index) {
	if ((index < 0) || (index > MAX_VIRTUAL_PORTS - 1)) {
		return kVPIndexOutOfRange;
	}
	return v_endpoints[index].filters;
}

static int setFilter(unsigned index, int32_t filters) {
	if ((index < 0) || (index > MAX_VIRTUAL_PORTS - 1)) {
		return kVPIndexOutOfRange;
	}
	v_endpoints[index].filters = filters;
	return noErr;
}

static int getChannelMask(unsigned index) {
	if ((index < 0) || (index > MAX_VIRTUAL_PORTS - 1)) {
		return kVPIndexOutOfRange;
	}
	return v_endpoints[index].channel_mask;
}

static int setChannelMask(unsigned index, int32_t masks) {
	if ((index < 0) || (index > MAX_VIRTUAL_PORTS - 1)) {
		return kVPIndexOutOfRange;
	}
	v_endpoints[index].channel_mask = masks;
	return noErr;
}

static int setLowWordAndHighWord(int32_t *value, int32_t lowWord, int32_t highWord) {
	if (currentPlatformsProcessorType != gestaltIntel) {
		((int *) value)[0] = highWord;
		((int *) value)[1] = lowWord;
	} else {
		((int *) value)[0] = lowWord;
		((int *) value)[1] = highWord;
	}
#ifdef _sqMacVirtualPort_debug_
	debugWrite("value=%llu\n", *value);
#endif _sqMacVirtualPort_debug_
	return noErr;
}


static Boolean isSystemExclusiveMessage (int count, int bufferPtr) {
	unsigned char *ptr;
	ptr = (unsigned char *) bufferPtr;
	unsigned char first = ((unsigned char) (*((int *) ptr)));
	return ((first == MIDI_SYSEX_START) && (((unsigned char) (*((int *) (ptr + count - 1)))) == MIDI_SYSEX_END));
}

static UInt64 callbackTimeStamp(void) {
	return callbackMidiMessage.timeStamp;
}

/* current time in nano sec */
static UInt64 getMIDIClock(void) {
	return AudioConvertHostTimeToNanos(AudioGetCurrentHostTime());
}

/* Called when all bytes of system-exclusive MIDI event have been sent, 
 or after the client has set complete to true. */
static void sysexCompletionProc(MIDISysexSendRequest *sysexreq) {
    free(sysexreq);
#ifdef _sqMacVirtualPort_debug_
	debugWrite("sysexCompletionProc done.\n");
#endif //_sqMacVirtualPort_debug_				
}

/*-----------------------------*/

static int getUnsignedLongLongOop(int32_t value) {
	if (value >= 0) {
		return composeUnsignedLongLongOopLowHigh(UInt64LowWord(value), UInt64HighWord(value));
	}
	return 0;
}

static int setUnsignedLongLongOop(int32_t oop, int32_t *value) {
	int err;
	unsigned int lowWord = 0;
	unsigned int highWord = 0;
	
	err = decomposeUnsignedLongLongOopLowHigh(oop, &lowWord, &highWord);
	if (err != noErr) {
		return err;
	}
	setLowWordAndHighWord(value, lowWord, highWord);
	return err;
}

/*-----------------------------
 statics end
 ----------------------------*/

void clearExternalFunctionAddress(void) {
	externalFunc = 0;
}

void closingModule() {
	void (*externalFuncAddress)(void) = 0;

	externalFuncAddress = lookupFunctionAddressfrom("clearExternalFunctionAddress", externalPluginName);
	if (externalFuncAddress != 0) {
		((void (*) (void)) externalFuncAddress)();
	}
}

/* callback functions */

int callbackStart(int semaphoreIndex) {
	callbackSemaphoreIndex = semaphoreIndex;
	initializeCallbackMidiMessage();
	return callbackSemaphoreIndex;
}

int callbackStop(void) {
	callbackSemaphoreIndex = 0;
	initializeCallbackMidiMessage();
	return callbackSemaphoreIndex;
}

int callbackLength(void) {
	return callbackMidiMessage.length;
}

unsigned int callbackTimeStampOop(void) {
	UInt64 timeStamp = callbackTimeStamp();
	return composeUnsignedLongLongOopLowHigh(UInt64LowWord(timeStamp), UInt64HighWord(timeStamp));
}

int callbackDestinationIndex(void) {
	return callbackMidiMessage.destinationIndex;
}

const char *callbackBytes(void) {
	return (char *) callbackMidiMessage.bytes;
}

int notifyCallbackStart(int semaphoreIndex) {
	notifySemaphoreIndex = semaphoreIndex;
	return notifySemaphoreIndex;
}

int notifyCallbackStop(void) {
	notifySemaphoreIndex = 0;
	return notifySemaphoreIndex;
}

const char *notifyCallbackMessage(void) {
	return notifyMessage;
}

/* returns an oop of current time in nano sec */
/* typedef UInt64 MIDITimeStamp; */
unsigned int getMIDIClockOop(void) {
    UInt64 nsec_time = getMIDIClock();
	return composeUnsignedLongLongOopLowHigh(UInt64LowWord(nsec_time), UInt64HighWord(nsec_time));
}

/* virtual port functions */

Boolean virtualPortIsSupported(void) {
	return checkSystemVersion(MINIMUM_SYSTEM_VERSION);
}

Boolean useNotifyMIDISystemStateChange(void) {
	return ownClient != NULL_REF;
}

/* virtual source & virtual destination */

/* return creatable sources number */
int countVirtualOutputPorts(void) {
	return MAX_VIRTUAL_OUTPUT;
}

/* return creatable destinations number */
int countVirtualInputPorts(void) {
	return MAX_VIRTUAL_INPUT;
}

/* return destination or source name by given index */
const char *getVirtualPortName(int index) {
	if (index < 0 || index > MAX_VIRTUAL_PORTS - 1) {
		return "";
	}
	if (isOpenedNoChekIndex(index)) {
		return getEndpointName(v_endpoints[index].endpoint);
	}
	return "";
}

/* dispose destination or source by given index */
int closeVirtualPort(int index) {
	if (index < 0 || index > MAX_VIRTUAL_PORTS - 1) {
		return kVPIndexOutOfRange;
	}
	if (isOpenedNoChekIndex(index)) {
		MIDIEndpointDispose(v_endpoints[index].endpoint);
		v_endpoints[index].endpoint = NULL_REF;
	}
	v_endpoints[index].runningStatus = 0;
	v_endpoints[index].latest_timeStamp = 0LLU;
	v_endpoints[index].filters = PM_FILT_ACTIVE;
	v_endpoints[index].channel_mask = RECEIVE_ALL_CHANNEL;
	v_endpoints[index].direction = VIRTUAL_UNDEFINED;
	return noErr;
}

void closeVirtualPorts(void) {
	int i;

	for (i = 0; i < MAX_VIRTUAL_PORTS; i++) {
		closeVirtualPort(i);
	}
}

/* initialize all sources & destinations */
int initializeVirtualPorts(void) {
    OSStatus result = noErr;
	
#ifdef _sqMacVirtualPort_debug_
	debugWrite("VirtualPortPlugin initializeVirtualPorts: getMIDIClientRef()=%ld\n", lookupFunctionAddressfrom("getMIDIClientRef", "PortMidiPlugin"));
#endif //_sqMacVirtualPort_debug_
	if (vp_is_inited) return noErr;
	setCurrentPlatformsProcessorType();

	//self flag: #TODO.
	if (lookupFunctionAddressfrom("getModuleName", externalPluginName) != 0) {
		if (lookupFunctionAddressfrom("getMIDIClientRef", externalPluginName) == 0) {
			return kVPPortMidiPluginDoesNotSupportVirtualPort;
		}
	}
	result = initializeExternalPluginIfNecessary();
	result = noErr;
	/* lookup getMIDIClientRef() from PortMidiPlugin */
	setExternalFunctionAddress();

	/* initialize virtual port endpoints */
	initializeInternalPorts();
	
	vp_is_inited = true;
	return result;
	
error_return:
#ifdef _sqMacVirtualPort_debug_
	debugWrite("initialize: error_return: result=%d\n", result);
#endif //_sqMacVirtualPort_debug_
	terminateVirtualPorts();
	return result;
}

/* dispose all sources & destinations */
int terminateVirtualPorts(void) {
    OSStatus result = noErr;
	
	closeVirtualPorts();
	if (ownClient != NULL_REF) {
		MIDIClientDispose(ownClient);
		ownClient = NULL_REF;
	}
	vp_is_inited = false;
	return result;
}

/* create destination by given index */
OSStatus openVirtualPort(int index, int direction) {
	OSStatus result;
	
	if (index < 0 || index > MAX_VIRTUAL_PORTS - 1) {
		return kVPIndexOutOfRange;
	}
	if (isOpenedNoChekIndex(index)) {
		return noErr;
	}
	MIDIClientRef client = lookupOrCreateMIDIClientRef();
	if (client == NULL_REF) {
		return kMIDIInvalidClient;
	}
	MIDIEndpointRef endpoint;
	if (direction == VIRTUAL_OUTPUT) {
		result = openVirtualOutputPort(client, &endpoint, index);
	} else if (direction == VIRTUAL_INPUT) {
		result = openVirtualInputPort(client, &endpoint, index);
	} else {
		return kVPBadDirection; 
	}
	if (result != noErr) {
		closeVirtualPort(index);
		return result;
	}
	setUniqueIDfor(endpoint);
	v_endpoints[index].endpoint = endpoint;
	v_endpoints[index].direction = direction;
	return result;
}

int isVirtualPortOpened(int index) {
	if (index < 0 || index > MAX_VIRTUAL_PORTS - 1) {
		return kVPIndexOutOfRange;
	}
	return isOpenedNoChekIndex(index);
}

/* write to source only, without opened output port */
OSStatus writeVirtualOutputPortAtFrom(int index, int count, int bufferPtr, unsigned long millisecondTime) {
	OSStatus result = noErr;
	
	if ((count <= 0) || (index < 0) || (index > MAX_VIRTUAL_PORTS - 1)) return noErr;
	if ((!(isOpenedNoChekIndex(index))) || (!(v_endpoints[index].direction == VIRTUAL_OUTPUT))) {
		return noErr;
	}
	
	MIDIEndpointRef endpoint = v_endpoints[index].endpoint;
	
	if (isSystemExclusiveMessage(count, bufferPtr)) {
		//0xf0, 0x00, 0x13, 0x37, 0x04, 0xf7
		MIDISysexSendRequest *sysexreq = malloc(sizeof(struct MIDISysexSendRequest) + count);
		sysexreq->destination = endpoint;
		sysexreq->data = ((unsigned char *) sysexreq) + sizeof(struct MIDISysexSendRequest);
		memcpy((void *) sysexreq->data, (unsigned char *) bufferPtr, count);
		sysexreq->bytesToSend = count;
		sysexreq->complete = false;
		sysexreq->completionProc = sysexCompletionProc;
		sysexreq->completionRefCon = &sysexreq;
		result = MIDISendSysex(sysexreq);
#ifdef _sqMacVirtualPort_debug_
		debugWrite("result=%d\n", result);
#endif //_sqMacVirtualPort_debug_
	} else {
		if (count == 3) {
			v_endpoints[index].runningStatus = ((unsigned char) (*((int *) bufferPtr)));
		} else if (count == 2) {
			unsigned char *ptr;
			ptr = (unsigned char *) bufferPtr;
			unsigned char byte1 = ((unsigned char) (*((int *) ptr++)));
			unsigned char byte2 = ((unsigned char) (*((int *) ptr++)));
			if (!(byte1 & MIDI_STATUS_MASK)) {
				/* running status */
				unsigned char runningStatus = v_endpoints[index].runningStatus;
				if (runningStatus) {
					Byte bytes[3];
					bytes[0] = runningStatus;
					bytes[1] = byte1;
					bytes[2] = byte2;
					bufferPtr = (int) bytes;
					count++;
#ifdef _sqMacVirtualPort_debug_
					int i;
					unsigned char *bytePtr = (unsigned char *) bytes;
					for (i = 0; i < count; i++) {
						debugWrite("bytePtr[%d]=%d\n", i, ((unsigned char) (*((int *) bytePtr++))));
					}
#endif //_sqMacVirtualPort_debug_				
				} else {
#ifdef _sqMacVirtualPort_debug_
					debugWrite("byte1=%d\n", byte1);
#endif //_sqMacVirtualPort_debug_
					return kVPDataTooShort;
				}
			} else {
				/*
				 * 2 bytes sending
				 * through
				 */
			}
		}		
		MIDIPacketList packetList;
		MIDIPacket *packet = MIDIPacketListInit(&packetList);
		MIDITimeStamp timeStamp = AudioGetCurrentHostTime() + kMillisecondScale * millisecondTime;
		packet = MIDIPacketListAdd(&packetList, sizeof(packetList), packet, timeStamp, count, (const Byte *) bufferPtr);
		if (!packet) {
			return kVPMIDIPacketListAddError;
		}
		result = MIDIReceived(endpoint, &packetList);
	}
	return result;
}

/* write to source with opened output port */
/* called from pmmacosx.c */
OSStatus writeVirtualOutputPortFrom(const MIDIPacketList *packetList) {
	OSStatus result;
	int i;
	for(i = 0; i < MAX_VIRTUAL_PORTS; i++) {
		if ((isOpenedNoChekIndex(i)) && (v_endpoints[i].direction == VIRTUAL_OUTPUT)) {
			result = MIDIReceived(v_endpoints[i].endpoint, packetList);
			if (result != noErr) {
				//Hey, hey what can i do?
				;
			}
		}
	}
	return result;
}

int getVirtualPortDirectionality(unsigned index) {
	if ((index < 0) || (index > MAX_VIRTUAL_PORTS - 1)) {
		return kVPIndexOutOfRange;
	}
	return v_endpoints[index].direction;
}

int getChannelMaskOop(unsigned index) {
	int32_t channel_mask = getChannelMask(index);
	if (channel_mask == kVPIndexOutOfRange) {
		return kVPIndexOutOfRange;
	}
	return composeUnsignedLongLongOopLowHigh(UInt64LowWord(channel_mask), UInt64HighWord(channel_mask));
}

int setChannelMaskOop(unsigned index, int32_t oop) {
	int32_t masks = getChannelMask(index);
	int err;
	
	if (masks == kVPIndexOutOfRange) {
		return kVPIndexOutOfRange;
	}
	err = setUnsignedLongLongOop(oop, &masks);
	if (err != noErr) {
		return err;
	}
	err = setChannelMask(index, masks);
	return err;
}

int getFilterOop(unsigned index) {
	int32_t filters = getFilter(index);
	if (filters == kVPIndexOutOfRange) {
		return kVPIndexOutOfRange;
	}
	return composeUnsignedLongLongOopLowHigh(UInt64LowWord(filters), UInt64HighWord(filters));
}

int setFilterOop(unsigned index, int32_t oop) {
	int32_t filters = getFilter(index);
	int err;
	
	if (filters == kVPIndexOutOfRange) {
		return kVPIndexOutOfRange;
	}
	err = setUnsignedLongLongOop(oop, &filters);
	if (err != noErr) {
		return err;
	}
	err = setFilter(index, filters);
	return err;
}

