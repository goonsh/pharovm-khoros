/*
 *  notification.c
 *  SoftSynthPlugin
 *
 *  Created by NISHIHARA Satoshi on 13/08/24.
 *  Copyright 2013 goonsh@gmail.com. All rights reserved.
 *
 */

#include "notification.h"

/*****************************************************************************
 editWindowDealloced
 
 called when anNativeUIController window is closing.
 @param audio unit
 @return void
 ******************************************************************************/
void editWindowDealloced(AudioUnit unit) {
	printf("editWindowDealloced\n");
}
