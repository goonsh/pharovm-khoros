/*
 *  interaction.c
 *  SoftSynthPlugin
 *
 *  Created by NISHIHARA Satoshi on 13/08/24.
 *  Copyright 2013 goonsh@gmail.com. All rights reserved.
 *
 */
#import <Cocoa/Cocoa.h>

#include <AssertMacros.h>

#include "interaction.h"

/*----------------------------------------------------------------------------
 inform
 
 show alert with OK button.
 @param messageText
 @param informativeText
 @return OK (default) = 1 / cancel (alternate) = 0 / none (other) = -1
 -----------------------------------------------------------------------------*/
int inform(const char *messageText, const char *informativeText) {
	return confirmOrOther(messageText, informativeText, NO, NO, NULL);
}

/*----------------------------------------------------------------------------
 confirm
 
 show alert with OK and Cancel button.
 @param messageText
 @param informativeText
 @return OK (default) = 1 / cancel (alternate) = 0 / none (other) = -1
 -----------------------------------------------------------------------------*/
int confirm(const char *messageText, const char *informativeText) {
	return confirmOrOther(messageText, informativeText, YES, NO, NULL);
}

/*----------------------------------------------------------------------------
 confirmOrOther
 
 show alert.
 @param cmessageText
 @param cinformativeText
 @param useCancel or not
 @param useOther or not
 @param name of otherButton, with useOther is set YES
 @return OK (default) = 1 / cancel (alternate) = 0 / none (other) = -1
 -----------------------------------------------------------------------------*/
int confirmOrOther(const char *cmessageText, const char *cinformativeText, Boolean useCancel, Boolean useOther, const char *otherButtonName) {
	NSString *messageText;
	NSString *informativeText;
	NSString *alternateButton = nil;
	NSString *otherButton = nil;
	
	if (cmessageText == NULL) cmessageText = "<none>";
	if (cinformativeText == NULL) cinformativeText = "<none>";
	messageText = [[[NSString alloc] initWithUTF8String:cmessageText] autorelease];
	informativeText = [[[NSString alloc] initWithUTF8String:cinformativeText] autorelease];
	if (useCancel) alternateButton = @"cancel";
	if (useOther) otherButton = [[[NSString alloc] initWithUTF8String:otherButtonName] autorelease];	
	NSAlert *alert = [NSAlert alertWithMessageText: messageText
									 defaultButton: @"OK" 
								   alternateButton: alternateButton 
									   otherButton: otherButton
						 informativeTextWithFormat: informativeText];
	// default = 1 / alternate = 0 / other = -1
	return [alert runModal];
}
