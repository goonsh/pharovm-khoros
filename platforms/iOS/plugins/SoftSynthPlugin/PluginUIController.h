//
//  PluginUIController.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/07/29.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//
//
#if !(TARGET_OS_IPHONE)

#ifndef __PluginUIController__
#define	__PluginUIController__

#import <Cocoa/Cocoa.h>
#import <AudioUnit/AudioUnit.h>
#import <AudioUnit/AudioUnitCarbonView.h>

// https://developer.apple.com/library/mac/releasenotes/Cocoa/AppKit.html
// Runtime Version Check
// 
// There are several ways to check for new features provided by the Cocoa frameworks at runtime. One is to look for a given new class or method dynamically, and not use it if not there. Another is to use the global variable NSAppKitVersionNumber (or, in Foundation, NSFoundationVersionNumber):
// double NSAppKitVersionNumber;
// #define NSAppKitVersionNumber10_0 577
// ...
// #define NSAppKitVersionNumber10_6 1038
// #define NSAppKitVersionNumber10_7 1138
// One typical use of this is to floor() the value, and check against the values provided in NSApplication.h. For instance:
// if (floor(NSAppKitVersionNumber) <= NSAppKitVersionNumber10_6) {
//   /* On a 10.6.x or earlier system
// } else if (floor(NSAppKitVersionNumber) <= NSAppKitVersionNumber10_7) {
//   /* On a 10.7 - 10.7.x system
// } else {
//   /* Mountain Lion or later system
// }
// Special cases or situations for version checking are also discussed in the release notes as appropriate. For instance some individual headers may also declare the versions numbers for NSAppKitVersionNumber where some bug fix or functionality is available in a given update, for example:
// #define NSAppKitVersionWithSuchAndSuchBadBugFix 582.1
#define	NSAppKitVersionNumber10_6 1038
#define	NSAppKitVersionNumber10_7 1138

//#define kViewStyleToolbarItemID		@"ViewStyle"
//#define kPresetToolbarItemID		@"Preset"
//#define UserDomain					@"User"
//#define LocalDomain					@"Local"

@interface PluginUIController : NSWindowController <NSWindowDelegate>
{
@private
	AudioUnit _audioUnit;
	NSView *_AUView;
	WindowRef _carbonWindow;
	AudioUnitCarbonView _carbonView;
	EventHandlerRef _carbonEventHandler;
	
	NSSize _audioUnitViewSize;
	int _audioUnitViewType;
//	
//	// toolbar
//	NSToolbar *toolbar;
//	// kViewStyleToolbarItemID
//	// the audiounit view style changing view (ends up in an NSToolbarItem)
//	NSView *viewStylePopUpView;
//	// this menu is the menuFormRepresentations of the toolbar items
//	NSMenu *viewStyleMenu;
//	// a state variable that keeps track of what style has been picked (Cocoa, Carbon, Generic)
//	NSInteger viewStylePicked;
//	// kPresetToolbarItemID
//	NSView *presetPopUpView;
//	NSMenu *presetMenu;
//	NSInteger presetPicked;
}

@property (getter=audioUnit) AudioUnit audioUnit;
@property (readonly, retain) NSView *auView;
@property (readonly) WindowRef carbonWindow;
@property (readonly) AudioUnitCarbonView carbonView;
@property NSSize audioUnitViewSize;
@property int audioUnitViewType;

//+ (CFStringRef)getAudioComponentName:(AudioComponent)component;
//+ (NSString *)getAudioUnitName:(AudioUnit)unit;
//+ (CFArrayRef)getPresets:(AudioUnit)unit;
//+ (OSStatus)setPreset:(AudioUnit)unit to:(UInt32)presetNumber;
//+ (OSStatus)setPreset:(AudioUnit)unit presetName:(CFStringRef)presetName presetNumber:(UInt32) presetNumber;
//+ (AUPreset)getCurrentPreset:(AudioUnit)unit;
//+ (NSString *)getCurrentPresetName:(AudioUnit)unit;

+ (BOOL)audioUnitHasCocoaView:(AudioUnit)unit;
+ (BOOL)audioUnitHasCarbonView:(AudioUnit)unit;
+ (id)showUI:(AudioUnit)unit;
+ (id)showUI:(AudioUnit)unit useToolbar:(BOOL)useToolbar;

//- (void)initWithCocoaViewForUnit:(AudioUnit)unit;
//- (void)initWithCarbonViewForUnit:(AudioUnit)unit;
//- (void)initWithCocoaGenericViewForUnit:(AudioUnit)unit;
//- (void)initWithCarbonGenericViewForUnit:(AudioUnit)unit;
//- (void)initWithAudioUnitCocoaView:(NSView *)audioUnitView;
//
- (id)initWithAudioUnit:(AudioUnit)unit;

//+ (NSArray *)getManufacturerAndNameFromAudioUnit:(AudioUnit)unit;
//- (NSArray *)getManufacturerAndNameFromAudioUnit:(AudioUnit)unit;
//+ (NSString *)getAudioUnitBasename:(AudioUnit)unit;
//- (NSString *)getAudioUnitBasename:(AudioUnit)unit;
//
//
//- (void)audioUnitChangedViewSize:(NSNotification *)notification;

@end


/*----------------------------------------------------------------------------
	c function prototypes
-----------------------------------------------------------------------------*/
int countOfUIControllers(void);
int closeUIControllerAt(int index);
void closeAllUIControllers(void);

#endif	//#ifndef __PluginUIController__
#endif	//#if !(TARGET_OS_IPHONE)
