//
//  PluginUIToolbarController.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/07/29.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//
//
#if !(TARGET_OS_IPHONE)

#ifndef __PluginUIToolbarController__
#define	__PluginUIToolbarController__

#import <Cocoa/Cocoa.h>
#import <AudioUnit/AudioUnit.h>
#import <AudioUnit/AudioUnitCarbonView.h>
#import "PluginUIController.h"

#define kViewStyleToolbarItemID		@"ViewStyle"
#define kPresetToolbarItemID		@"Preset"
#define FactoryDomain				@"Factory"
#define UserDomain					@"User"
#define LocalDomain					@"Local"

@interface PluginUIToolbarController : PluginUIController <NSToolbarDelegate>
{
@private
//	AudioUnit _audioUnit;
//	NSView *_AUView;
//	WindowRef _carbonWindow;
//	AudioUnitCarbonView _carbonView;
//	EventHandlerRef _carbonEventHandler;
//	
//	NSSize _audioUnitViewSize;
//	int _audioUnitViewType;
	
	// toolbar
	NSToolbar *_toolbar;
	// kViewStyleToolbarItemID
	// the audiounit view style changing view (ends up in an NSToolbarItem)
	NSView *_viewStylePopUpView;
	// this menu is the menuFormRepresentations of the toolbar items
	NSMenu *_viewStyleMenu;
	// a state variable that keeps track of what style has been picked (Cocoa, Carbon, Generic)
	NSInteger _viewStylePicked;
	// kPresetToolbarItemID
	NSView *_presetPopUpView;
	NSMenu *_presetMenu;
	NSInteger _presetPicked;
	
	NSMutableArray *_presetsTree;
}

//+ (CFStringRef)getAudioComponentName:(AudioComponent)component;
//+ (NSString *)getAudioUnitName:(AudioUnit)unit;
//+ (CFArrayRef)getPresets:(AudioUnit)unit;
//+ (OSStatus)setPreset:(AudioUnit)unit to:(UInt32)presetNumber;
//+ (OSStatus)setPreset:(AudioUnit)unit presetName:(CFStringRef)presetName presetNumber:(UInt32) presetNumber;
//+ (AUPreset)getCurrentPreset:(AudioUnit)unit;
//+ (NSString *)getCurrentPresetName:(AudioUnit)unit;
//
//+ (BOOL)audioUnitHasCocoaView:(AudioUnit)unit;
//+ (BOOL)audioUnitHasCarbonView:(AudioUnit)unit;
//+ (id)showUI:(AudioUnit)unit;
//+ (id)showUI:(AudioUnit)unit title:(NSString *)windowTitle forceGeneric:(BOOL)forceGeneric;
//
//- (void)initWithCocoaViewForUnit:(AudioUnit)unit;
//- (void)initWithCarbonViewForUnit:(AudioUnit)unit;
//- (void)initWithCocoaGenericViewForUnit:(AudioUnit)unit;
//- (void)initWithCarbonGenericViewForUnit:(AudioUnit)unit;
//- (void)initWithAudioUnitCocoaView:(NSView *)audioUnitView;
//
//- (id)initWithAudioUnit:(AudioUnit)unit forceGeneric:(BOOL)useGeneric;
//
//- (void)audioUnitChangedViewSize:(NSNotification *)notification;

//@property (readonly) WindowRef carbonWindow;
//@property NSSize audioUnitViewSize;
//@property (getter=audioUnit) AudioUnit audioUnit;
@property (retain) NSToolbar *toolbar;
@property (retain) NSView *viewStylePopUpView;
@property (retain) NSMenu *viewStyleMenu;
@property NSInteger viewStylePicked;
@property (retain) NSView *presetPopUpView;
@property (retain) NSMenu *presetMenu;
@property NSInteger presetPicked;
@property (retain) NSMutableArray *presetsTree;

@end

#endif	//#ifndef __PluginUIController__
#endif	//#if !(TARGET_OS_IPHONE)
