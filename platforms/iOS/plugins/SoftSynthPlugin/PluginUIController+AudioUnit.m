//
//  PluginUIController+AudioUnit.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/16.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import "PluginUIController+AudioUnit.h"

@implementation PluginUIController (AudioUnit)

//**************************************************************************************************
//    getAudioComponentName:
//**************************************************************************************************
+ (CFStringRef)getAudioComponentName:(AudioComponent)component
{
	OSStatus result;
	CFStringRef nameRef;
	
	require_noerr(result = AudioComponentCopyName(component, &nameRef), end);
	if (nameRef == NULL || 
		(CFStringCompare(nameRef, CFSTR(""), 0) == kCFCompareEqualTo)) {
		nameRef = CFSTR("<unknown>");
	}
	return nameRef;
	
end:
	return CFSTR("<unknown>");
}

//**************************************************************************************************
//    getAudioUnitName:
//
//		get the name of autio unit
//		@param AudioUnit audioUnit
//		@return (NSString *) name of given autio unit
//**************************************************************************************************
+ (NSString *)getAudioUnitName:(AudioUnit)unit
{
	return (NSString *) [self getAudioComponentName:AudioComponentInstanceGetComponent(unit)];
}

//**************************************************************************************************
//    getAudioUnitBasenameByIndex:
//
//		get audiounit basename (after $:) for given autio unit
//		@param AudioUnit audioUnit
//		@return (NSString *) basename of AudioUnit
//**************************************************************************************************
+ (NSString *)getAudioUnitBasenameByIndex:(AudioUnit)unit
{
	NSString *audioUnitName = [self getAudioUnitName:unit];
	NSUInteger length = [audioUnitName length];
	NSUInteger foundPos = length;
	for (NSUInteger i = length - 1; i > 0; i--) {
		unichar c = [audioUnitName characterAtIndex: i];
		if (c == ':') {
			foundPos = i;
			break;
		}
	}
	NSLog(@"getCurrentPresetBasename: %@", [audioUnitName substringFromIndex:foundPos]);
	return [audioUnitName substringFromIndex:foundPos];
}

//**************************************************************************************************
//    getAudioUnitBasename:
//
//		get trimmed audiounit basename (after $:) for given autio unit
//		@param AudioUnit audioUnit
//		@return (NSString *) basename of AudioUnit
//**************************************************************************************************
+ (NSString *)getAudioUnitBasename:(AudioUnit)unit
{
	NSString *audioUnitName = [self getAudioUnitName:unit];
	NSArray *tokens = [audioUnitName componentsSeparatedByString:@":"];
	NSString *lastObject = [tokens lastObject];
	if (lastObject == nil) {
		return nil;
	}
	return [lastObject stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSArray *)getManufacturerAndNameFromAudioUnit:(AudioUnit)unit
{
	NSString *unitName = [self getAudioUnitName:unit];
	if (unitName == nil) {
		return nil;
	}
	NSString *auManufacturer;
	NSString *auName;
	NSUInteger colonIndex = [unitName rangeOfString:@":" options:NSLiteralSearch].location;
	if (colonIndex == NSNotFound) {
		return nil;
	}
	auManufacturer = [[unitName substringToIndex:colonIndex] copy];
	colonIndex++;
	NSCharacterSet *whitespaceCharacters = [NSCharacterSet whitespaceCharacterSet];
	while ([whitespaceCharacters characterIsMember:[unitName characterAtIndex:colonIndex]]) {
		++colonIndex;
	}
	auName = [[unitName substringFromIndex:colonIndex] copy];
	return [NSArray arrayWithObjects:auManufacturer, auName, nil];
}

//--------------------------------------------------------------------------------------------------
//    getAudioUnitName:
//--------------------------------------------------------------------------------------------------
- (NSString *)getAudioUnitName:(AudioUnit)unit
{
	return [[self class] getAudioUnitName:unit];
}

//--------------------------------------------------------------------------------------------------
//    getAudioComponentName:
//--------------------------------------------------------------------------------------------------
- (CFStringRef)getAudioComponentName:(AudioComponent)component
{
	return [[self class] getAudioComponentName:component];
}

//--------------------------------------------------------------------------------------------------
//    getAudioUnitBasename:
//--------------------------------------------------------------------------------------------------
- (NSString *)getAudioUnitBasename:(AudioUnit)unit
{
	return [[self class] getAudioUnitBasename:unit];
}

//--------------------------------------------------------------------------------------------------
//    getManufacturerAndNameFromAudioUnit:
//--------------------------------------------------------------------------------------------------
- (NSArray *)getManufacturerAndNameFromAudioUnit:(AudioUnit)unit
{
	return [[self class] getManufacturerAndNameFromAudioUnit:unit];
}

@end
