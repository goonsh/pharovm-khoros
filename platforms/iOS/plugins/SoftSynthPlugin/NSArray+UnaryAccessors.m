//
//  NSArray+UnaryAccessors.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/08.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import "NSArray+UnaryAccessors.h"


@implementation NSArray (UnaryAccessors)

- (id)first
{
	return [self objectAtIndex:0];
}

- (id)second
{
	return [self objectAtIndex:1];
}

- (id)last
{
	return [self lastObject];
}

@end
