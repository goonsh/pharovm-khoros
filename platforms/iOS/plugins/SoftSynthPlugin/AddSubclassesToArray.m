//
//  AddSubclassesToArray.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/17.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

/****************************************************************************/
/* 
Cocoa with Love: Getting the subclasses of an Objective-C class
http://www.cocoawithlove.com/2010/01/getting-subclasses-of-objective-c-class.html
 */
/****************************************************************************/

/*
 <h4>The fast and dirty hack approach</h4>
 
 <p>The function I just gave is the correct approach to use in a program. But I wouldn't feel like I'd made an interesting post if I didn't also show a completely different, totally unsafe approach too.</p>
 
 <p>This approach will require the Objective-C Runtime 2.0 (that's the one used in Mac OS X 64-bit and on the iPhone). In the new version of the runtime, a class actually contains a direct link to its subclasses.</p>
 
 <p>From <a href="http://opensource.apple.com/source/objc4/objc4-437/runtime/objc-runtime-new.h">objc-runtime-new.h in Apple's opensource repository</a>, a class is declared as follows:</p>
 
 <p>This means that we can traverse along the <code>firstSubclass</code> and <code>nextSiblingClass</code> members to reach all of the subclasses of a parent class without needing to read every class in the runtime.</p>
 
 <p>A depth-first recursive traversal then gives the full list of subclasses for a class:</p>
 
 <p>However, while highly efficient compared to traversing the whole list of classes, this approach cannot be safely used. According to <a href="http://opensource.apple.com/source/objc4/objc4-437/runtime/objc-runtime-new.m">objc-runtime-new.m</a>, all access to the <code>data</code> member of a <code>class_t</code> must be protected by the <code>runtimeLock</code> &mdash; which is inaccessible outside the runtime library itself.</p>
 
 <p>Since the lock is inaccessible, it means that this approach cannot be made safe, since any thread (including threads automatically started by Cocoa) could cause you to crash.</p>
 
 <p>It was a fun experiment and I hope that Apple include a function to do this safely in the future.</p>
 
 */

typedef void *Cache;

#import "AddSubclassesToArray.h"

void AddSubclassesToArray(Class parentClass, NSMutableArray *subclasses)
{
	struct class_t *internalRep = (struct class_t *)parentClass;
	
	// Traverse depth first
	Class subclass = (Class)internalRep->data->firstSubclass;
	while (subclass) {
		[subclasses addObject:subclass];
		AddSubclassesToArray(subclass, subclasses);
		
		// Then traverse breadth-wise
		struct class_t *subclassInternalRep = (struct class_t *)subclass;
		subclass = (Class)subclassInternalRep->data->nextSiblingClass;
	}
}
