//
//  AddSubclassesToArray.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/17.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//#import <objc/objc-runtime.h>
#import "objc-runtime-new.h"

void AddSubclassesToArray(Class parentClass, NSMutableArray *subclasses);
