//
//  PluginUIController+Interaction.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/09.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PluginUIController.h"

@interface PluginUIController (Interaction)

- (NSAlert *)inform:(NSString *)messageText informativeText:(NSString *)informativeText;
- (NSAlert *)confirm:(NSString *)messageText informativeText:(NSString *)informativeText;
- (NSAlert *)confirmOrOther:(NSString *)messageText 
			informativeText:(NSString *)informativeText 
				  useCancel:(BOOL)useCancel 
				   useOther:(BOOL)useOther 
			otherButtonName:(NSString *)otherButtonName;

@end
