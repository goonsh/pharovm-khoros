//
//  PluginUIToolbarController+Interaction.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/09.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import "PluginUIToolbarController+Interaction.h"


@implementation PluginUIToolbarController (Interaction)

- (NSAlert *)requestDomain:(NSString *)messageText 
		   informativeText:(NSString *)informativeText 
			   initialText:(NSString *)initialText
{
	CGFloat labelWidth = 100.0f;
	CGFloat fieldWidth = 250.0f;
//	CGFloat labelHeight = 15.0f;
	CGFloat labelHeight = 22.0f;
	CGFloat fieldHeight = fieldWidth;
	CGFloat popupHeight = 25.0f;
	CGFloat accessoryViewHeight = 50.0f;
	
	NSRect accessoryViewRect = NSMakeRect(0,0,(labelWidth + fieldWidth),accessoryViewHeight);
	// prepare base view
	NSTextView *accessoryView = [[NSTextView alloc] initWithFrame:accessoryViewRect];
	[accessoryView setDrawsBackground:NO];
	// create preset name label
	NSText *nameLabel = [[NSText alloc] initWithFrame:NSMakeRect(0,0,labelWidth,labelHeight)];
	[nameLabel insertText:@"Preset name:"];
	[nameLabel setEditable:NO];
	[nameLabel setDrawsBackground:NO];
	// create preset name field
	NSTextField *inputField = [[NSTextField alloc] initWithFrame:NSMakeRect(labelWidth,0,fieldWidth,labelHeight)];
	if (initialText == nil) {
		initialText = @"preset name";
	}
	[inputField setStringValue:initialText];
	[inputField setEditable:YES];
	[inputField setBezeled:YES];
//	[inputField setBezelStyle:NSTextFieldSquareBezel];
	[inputField setBezelStyle:NSTextFieldRoundedBezel];
	// create popup label
	NSText *locationLabel = [[NSText alloc] initWithFrame:NSMakeRect(0,popupHeight,fieldWidth,fieldHeight)];
	[locationLabel insertText:@"Preset location:"];
	[locationLabel setEditable:NO];
	[locationLabel setDrawsBackground:NO];
	// create popup
	NSPopUpButton *popup = [[NSPopUpButton alloc] initWithFrame:NSMakeRect(labelWidth,popupHeight,fieldWidth,popupHeight) 
													  pullsDown:NO];
	NSArray *itemTitles = [NSArray arrayWithObjects:UserDomain, LocalDomain, nil];
	[popup addItemsWithTitles:itemTitles];
	
	// create alert
	NSAlert *alert = [[NSAlert alloc] init];
	[alert setAlertStyle:NSInformationalAlertStyle];
	[alert addButtonWithTitle:@"OK"];
	[alert addButtonWithTitle:@"Cancel"];
	if (messageText == nil) {
		messageText = @"Request";
	}
	[alert setMessageText:messageText];
	if (informativeText == nil) {
		informativeText = @"";
	}
	[alert setInformativeText:informativeText];
	[alert layout];

	[accessoryView addSubview:nameLabel];
	[accessoryView addSubview:inputField];
	[accessoryView addSubview:locationLabel];
	[accessoryView addSubview:popup];

	[alert setAccessoryView:accessoryView];
	return alert;
}

@end
