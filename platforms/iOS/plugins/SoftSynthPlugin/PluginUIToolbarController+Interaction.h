//
//  PluginUIToolbarController+Interaction.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/09.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "PluginUIToolbarController.h"

@interface PluginUIToolbarController (Interaction)

- (NSAlert *)requestDomain:(NSString *)messageText 
		   informativeText:(NSString *)informativeText 
			   initialText:(NSString *)initialText;

@end
