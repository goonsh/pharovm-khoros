//
//  NSArray+UnaryAccessors.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/08.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSArray (UnaryAccessors)

- (id)first;
- (id)second;
- (id)last;

@end
