//
//  Log.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/26.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <objc/objc-runtime.h>

#import "Log.h"

NSString *GSDebugFunctionMsg(const char *func, const char *file, int line, NSString *fm);
NSString *GSDebugMethodMsg(id obj, SEL sel, const char *file, int line, NSString *fmt);
BOOL GSDebugSet(NSString *level);


@implementation NSProcessInfo(GNUstepBase)
static NSMutableSet	*_debug_set = nil;
static BOOL debugTemporarilyDisabled = NO;

- (NSMutableSet *) debugSet
{
	if (_debug_set == nil)
    {
		int		argc = [[self arguments] count];
		NSMutableSet	*mySet;
		int		i;
		
		mySet = [NSMutableSet new];
		for (i = 0; i < argc; i++)
		{
			NSString	*str = [[self arguments] objectAtIndex: i];
			
			if ([str hasPrefix: @"--GNU-Debug="])
			{
				[mySet addObject: [str substringFromIndex: 12]];
			}
		}
		_debug_set = mySet;
    }
	return _debug_set;
}

- (BOOL) debugLoggingEnabled
{
	if (debugTemporarilyDisabled == YES)
    {
		return NO;
    }
	else
    {
		return YES;
    }
}

- (void) setDebugLoggingEnabled: (BOOL)flag
{
	if (flag == NO)
    {
		debugTemporarilyDisabled = YES;
    }
	else
    {
		debugTemporarilyDisabled = NO;
    }
}

@end

NSString *GSDebugFunctionMsg(const char *func, const char *file, int line, NSString *fmt)
{
	NSString *message;
	
	message = [NSString stringWithFormat: @"File %s: %d. In %s %@",
			   file, line, func, fmt];
	return message;
}

NSString *GSDebugMethodMsg(id obj, SEL sel, const char *file, int line, NSString *fmt)
{
	NSString	*message;
	Class		cls = [obj class];
	char		c = '-';
	
	if (class_isMetaClass(cls)) {
		cls = (Class)obj;
		c = '+';
    }
	message = [NSString stringWithFormat: @"File %s: %d. In [%@ %c%@] %@",
			   file, line, NSStringFromClass(cls), c, NSStringFromSelector(sel), fmt];
	return message;
}

BOOL GSDebugSet(NSString *level)
{
	static IMP debugImp = 0;
	static SEL debugSel;
	
	if (debugTemporarilyDisabled == YES) {
		return NO;
    }
	if (debugImp == 0) {
		debugSel = @selector(member:);
		if (_debug_set == nil) {
			[[NSProcessInfo processInfo] debugSet];
		}
		debugImp = [_debug_set methodForSelector: debugSel];
		if (debugImp == 0) {
			fprintf(stderr, "Unable to set up with [NSProcessInfo-debugSet]\n");
			return NO;
		}
    }
	if ((*debugImp)(_debug_set, debugSel, level) == nil) {
		return NO;
    }
	return YES;
}


