//
//  PluginUIToolbarController.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/07/29.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//
//	2013/08/14 fixed: get CocoaViewBundlePath through CFURLCopyPath.
//	select "AUDelay" will cause crash.
//	select "MiniSpillage" will cause crash.
//	select "AUMultibandCompressor" will cause crash.
//	comment out CFRelease(cocoaViewInfo->mCocoaAUViewBundleLocation); at getCocoaView()
//	it can show edit view for "AUDelay" and "AUMultibandCompressor"
//

#import <CoreAudioKit/CoreAudioKit.h>
#import <AudioUnit/AUCocoaUIView.h>
#import <CoreAudioKit/AUGenericView.h>

#include <Carbon/Carbon.h>

#import "PluginUIController+Interaction.h"
#import "PluginUIController+AudioUnit.h"
#import "PluginUIToolbarController.h"
#import "PluginUIToolbarController+Interaction.h"
#import "PluginUIToolbarController+Presets.h"

#import "NSArray+UnaryAccessors.h"
#import "NSString+Finding.h"

#import <AssertMacros.h>
#import "sqMacSoftSynth.h"
#import "debug.h"
#import "interaction.h"
#import "notification.h"

#if !(TARGET_OS_IPHONE)

// private methods
@interface PluginUIToolbarController (Private)
- (NSString *)currentPresetFileName;
//+ (NSString *)getCurrentPresetName:(AudioUnit)unit;
//- (NSString *)getCurrentPresetName:(AudioUnit)unit;
- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo;
- (void) setupToolbar;

@end

#pragma mark -
//**************************************************************************************************
@implementation PluginUIToolbarController
//**************************************************************************************************
//{
//	NSMutableArray *_presetsTree;
//}

@synthesize toolbar = _toolbar;
@synthesize viewStylePopUpView = _viewStylePopUpView;
@synthesize viewStyleMenu = _viewStyleMenu;
@synthesize viewStylePicked = _viewStylePicked;
@synthesize presetPopUpView = _presetPopUpView;
@synthesize presetMenu = _presetMenu;
@synthesize presetPicked = _presetPicked;
@synthesize presetsTree = _presetsTree;

static const id presetObject = @"aupreset";

//**************************************************************************************************
//    getCurrentPreset:
//
//		get current preset for given autio unit
//		@param AudioUnit audioUnit
//		@return AUPreset
//**************************************************************************************************
//+ (AUPreset)getCurrentPreset:(AudioUnit)unit
//{
//	NSLog(@"getCurrentPreset: has been called.");
//
//	AUPreset currentPreset;
//	UInt32 size = sizeof(currentPreset);
//    OSStatus result = AudioUnitGetProperty(unit,
//										   kAudioUnitProperty_PresentPreset,
//										   kAudioUnitScope_Global,
//										   0,
//										   &currentPreset,
//										   &size);
//	if (result == kAudioUnitErr_InvalidProperty) {
//		size = sizeof(currentPreset);
//		result = AudioUnitGetProperty(unit,
//									  kAudioUnitProperty_CurrentPreset,
//									  kAudioUnitScope_Global,
//									  0,
//									  &currentPreset,
//									  &size);
//		if (result == noErr) {
//			if (currentPreset.presetName) {
//				CFRetain((currentPreset.presetName));
//			}
//		}
//	}
//	return currentPreset;
//}
//
//**************************************************************************************************
//    getCurrentPresetName:
//
//		get current preset name for given autio unit
//		@param AudioUnit audioUnit
//		@return (NSString *) name of AUPreset
//**************************************************************************************************
//+ (NSString *)getCurrentPresetName:(AudioUnit)unit
//{
//	AUPreset preset = [self getCurrentPreset: unit];
//	return (NSString *) preset.presetName;
//}


//**************************************************************************************************
//    notifyAUParameterListenerOfParameterChanges:
//
//		Notify listeners of a past parameter change.
//		@param AudioUnit audioUnit
//		@return none
//**************************************************************************************************
//+ (void)notifyAUParameterListenerOfParameterChanges: (AudioUnit)unit
//{
//	AudioUnitParameter changedUnit;
//	changedUnit.mAudioUnit = unit;
//	changedUnit.mParameterID = kAUParameterListener_AnyParameter;
//	OSStatus result = AUParameterListenerNotify(NULL, NULL, &changedUnit);
//	if (result != noErr) {
//		NSLog(@"AUParameterListenerNotify failed: %i", result);
//	}
//}

//--------------------------------------------------------------------------------------------------
//    audioUnotifyAUParameterListenerOfParameterChanges:
//		
//		Notify listeners of a past parameter change.
//		@param AudioUnit audioUnit
//		@return none
//--------------------------------------------------------------------------------------------------
//- (void) notifyAUParameterListenerOfParameterChanges: (AudioUnit)unit
//{
//	[[self class] notifyAUParameterListenerOfParameterChanges:unit];
//}

//--------------------------------------------------------------------------------------------------
//    getCurrentPreset:
//--------------------------------------------------------------------------------------------------
//- (AUPreset)getCurrentPreset:(AudioUnit)unit
//{
//	return [[self class] getCurrentPreset:unit];
//}

- (NSMutableArray *)presetsTree
{
	if (_presetsTree == nil) {
		_presetsTree = [[NSMutableArray alloc] init];
	}
	return _presetsTree;
}

//--------------------------------------------------------------------------------------------------
//	dealloc
//--------------------------------------------------------------------------------------------------
- (void) dealloc
{
	// 	[[NSNotificationCenter defaultCenter] 
	// 									removeObserver:self
	// 									name:NSViewFrameDidChangeNotification
	// 									object:_AUView];
	if ([self toolbar]) {
		[[self toolbar] release];
	}
	if ([self viewStylePopUpView]) {
		[[self viewStylePopUpView] release];
	}
	if ([self viewStyleMenu]) {
		[[self viewStyleMenu] release];
	}
	[[self presetsTree] release];
	_presetsTree = nil;
	[super dealloc];
}

- (id)basicInitialize
{
	NSLog(@"basicInitialize: viewStylePicked=%d", _viewStylePicked);
	_viewStylePicked = 0;
	NSLog(@"basicInitialize: presetPicked=%d", _presetPicked);
	_presetPicked = 0;
	return self;
}

#pragma mark designated initializer
//--------------------------------------------------------------------------------------------------
//	initWithAudioUnit:
//
//	This method is the designated initializer for PluginUIController.
//
//--------------------------------------------------------------------------------------------------
- (id)initWithAudioUnit:(AudioUnit)unit
{
	if ((self = [super initWithAudioUnit:unit]) == nil) {
		return nil;
	}
	[self setupToolbar];
	return self;
}

- (float) getToolbarHeightOfWindow:(NSWindow *)window
{ 
    NSToolbar *theToolbar = [window toolbar]; 
    float toolbarHeight = 0.0f; 
    
    if (theToolbar && [theToolbar isVisible]) { 
        NSRect windowFrame = [NSWindow contentRectForFrameRect:[window frame]
                                                     styleMask:[window styleMask]]; 
        toolbarHeight = NSHeight(windowFrame) - NSHeight([[window contentView] frame]); 
    } 
    
    return toolbarHeight; 
}

//--------------------------------------------------------------------------------------------------
// Factory method to create autoreleased NSToolbarItems.
//
// All NSToolbarItems have a unique identifer associated with them, used to tell your delegate/controller
// what toolbar items to initialize and return at various points.  Typically, for a given identifier,
// you need to generate a copy of your "master" toolbar item, and return it autoreleased.  The function
// creates an NSToolbarItem with a bunch of NSToolbarItem paramenters.
//
// It's easy to call this function repeatedly to generate lots of NSToolbarItems for your toolbar.
// 
// The label, palettelabel, toolTip, action, and menu can all be nil, depending upon what you want
// the item to do.
//--------------------------------------------------------------------------------------------------
- (NSToolbarItem *)toolbarItemWithIdentifier:(NSString *)identifier
                                       label:(NSString *)label
                                 paleteLabel:(NSString *)paletteLabel
                                     toolTip:(NSString *)toolTip
                                      target:(id)target
                                 itemContent:(id)imageOrView
                                      action:(SEL)action
                                        menu:(NSMenu *)menu
{
	NSLog(@"toolbarItemWithIdentifier:label:paleteLabel:toolTip:target:itemContent:action:menu: has been called");
	// here we create the NSToolbarItem and setup its attributes in line with the parameters
	NSToolbarItem *item = [[[NSToolbarItem alloc] initWithItemIdentifier:identifier] autorelease];
	
	[item setLabel:label];
	[item setPaletteLabel:paletteLabel];
	[item setToolTip:toolTip];
	[item setTarget:target];
	[item setAction:action];
    
	// Set the right attribute, depending on if we were given an image or a view
	if ([imageOrView isKindOfClass:[NSImage class]]) {
		[item setImage:imageOrView];
	} else if ([imageOrView isKindOfClass:[NSView class]]) {
		[item setView:imageOrView];
	} else {
		//assert(!"Invalid itemContent: object");
		NSLog(@"Invalid itemContent: object");
	}
	
	
	// If this NSToolbarItem is supposed to have a menu "form representation" associated with it
	// (for text-only mode), we set it up here.  Actually, you have to hand an NSMenuItem
	// (not a complete NSMenu) to the toolbar item, so we create a dummy NSMenuItem that has our real
	// menu as a submenu.
	//
	if (menu != nil) {
		// we actually need an NSMenuItem here, so we construct one
		NSMenuItem *mItem = [[[NSMenuItem alloc] init] autorelease];
		[mItem setSubmenu:menu];
		[mItem setTitle:label];
		[mItem setToolTip:toolTip];
		[item setMenuFormRepresentation:mItem];
	}
	
	return item;
}

#pragma mark -
#pragma mark Actions
//--------------------------------------------------------------------------------------------------
// This action is called from the change font style toolbar item, both from the NSPopUpButton in the
// custom view, and from the menuFormRepresentation menu.
//--------------------------------------------------------------------------------------------------
- (IBAction)changeViewStyle:(id)sender
{
	NSLog(@"changeViewStyle: has been called");
	NSInteger itemIndex;
	
	// If the sender is an NSMenuItem then this is the menuFormRepresentation.  Otherwise, we are
	// being called from the NSPopUpButton.  We need to check this to find out how to calculate the
	// index of the picked menu item.
	//
	if ([NSStringFromClass([sender class]) isEqual:@"NSMenuItem"]) {
		// for ordinary NSMenus, the title is item #0, so we have to offset things
		itemIndex = [[sender menu] indexOfItem:sender];
	} else {
		// this is an NSPopUpButton, so the first useful item really is #0
		itemIndex = [sender indexOfSelectedItem];
	}
	NSLog(@"changeViewStyle: %@ %d %@", sender, itemIndex, [sender title]);
	
	// 	[fontSizeField takeIntegerValueFrom:fontSizeStepper];
	// 	theFont = [contentView font];
	
	// set the font properties depending upon what was selected
	switch (itemIndex) {
		case 0:
		{
			// 			theFont = [[NSFontManager sharedFontManager] convertFont:theFont toNotHaveTrait:NSItalicFontMask];
			// 			theFont = [[NSFontManager sharedFontManager] convertFont:theFont toNotHaveTrait:NSBoldFontMask];
			break;
		}
		case 1:
		{
			// 			theFont = [[NSFontManager sharedFontManager] convertFont:theFont toNotHaveTrait:NSItalicFontMask];
			// 			theFont = [[NSFontManager sharedFontManager] convertFont:theFont toHaveTrait:NSBoldFontMask];
			break;
        }
		case 2:
		{
			// 			theFont = [[NSFontManager sharedFontManager] convertFont:theFont toNotHaveTrait:NSBoldFontMask];
			// 			theFont = [[NSFontManager sharedFontManager] convertFont:theFont toHaveTrait:NSItalicFontMask];
			break;
		}
	}
	
	// make sure the fontStylePicked variable matches the menu selection plus 1, which also matches
	// the menu item tags in the menuFormRepresentation (see the menu in Interface Builder), so
	// that -validateMenuItem: can do its work. 
	//
//	viewStylePicked = itemIndex + 1;
	[self setViewStylePicked:itemIndex + 1];
	//	[contentView setFont:theFont range:[contentView selectedRange]];
}

- (IBAction)changePreset:(id)sender
{
	NSLog(@"changePreset: has been called");
	NSInteger itemIndex;
	NSString *presetName = nil;
	
	// If the sender is an NSMenuItem then this is the menuFormRepresentation.  Otherwise, we are
	// being called from the NSPopUpButton.  We need to check this to find out how to calculate the
	// index of the picked menu item.
	//
	if ([NSStringFromClass([sender class]) isEqual:@"NSMenuItem"]) {
		// for ordinary NSMenus, the title is item #0, so we have to offset things
		itemIndex = [[sender menu] indexOfItem:sender];
		presetName = [sender title];
	} else {
		// this is an NSPopUpButton, so the first useful item really is #0
		itemIndex = [sender indexOfSelectedItem];
	}
	NSLog(@"changePreset: %@ [%d] %@", sender, itemIndex, [sender title]);
//	[[self class] setPreset: [self audioUnit] presetName:(CFStringRef)[sender title] presetNumber:itemIndex];
	[[self class] setPreset: [self audioUnit] named:(CFStringRef)presetName];
//	[[self class] setPreset: [self audioUnit] to:itemIndex];
	[self setPresetPicked:itemIndex];
}

- (void) loadCustomPresetFromURL:(NSURL *)presetURL
{
	NSParameterAssert(nil != presetURL);
	
	NSError *error = nil;
	NSData *xmlData = [NSData dataWithContentsOfURL:presetURL options:NSUncachedRead error:&error];
	
	if (nil == xmlData) {
		NSLog(@"SoftSynthPlugin: Unable to load preset from %@ (%@)", presetURL, error);
		return;
	}
	
	NSString *errorString = nil;
	NSPropertyListFormat plistFormat = NSPropertyListXMLFormat_v1_0;
	id classInfoPlist = [NSPropertyListSerialization propertyListFromData:xmlData 
														 mutabilityOption:NSPropertyListImmutable 
																   format:&plistFormat 
														 errorDescription:&errorString];
	
	if (nil != classInfoPlist) {
		ComponentResult err = AudioUnitSetProperty([self audioUnit],
												   kAudioUnitProperty_ClassInfo, 
												   kAudioUnitScope_Global, 
												   0, 
												   &classInfoPlist, 
												   sizeof(classInfoPlist));
		if (noErr != err) {
			NSLog(@"SoftSynthPlugin: AudioUnitSetProperty(kAudioUnitProperty_ClassInfo) failed: %i", err);
			return;
		}
		
		[self notifyAUParameterListenerOfParameterChanges:[self audioUnit]];
	} else {
		NSLog(@"SoftSynthPlugin: Unable to create property list for AU class info: %@", errorString);
	}
	[errorString release];
}

- (void) saveCustomPresetToURL:(NSURL *)presetURL presetName:(NSString *)presetName
{
	NSParameterAssert(nil != presetURL);
	
	// First set the preset's name
	if (presetName == nil) {
		presetName = [[[presetURL path] lastPathComponent] stringByDeletingPathExtension];
	}
	NSLog(@"presetName: %@", presetName);
	AUPreset preset;
	preset.presetNumber = -1;
	preset.presetName = (CFStringRef)presetName;
	
	ComponentResult err = AudioUnitSetProperty([self audioUnit], 
											   kAudioUnitProperty_PresentPreset,
											   kAudioUnitScope_Global, 
											   0, 
											   &preset, 
											   sizeof(preset));
	if (noErr != err) {
		NSLog(@"SoftSynthPlugin: AudioUnitSetProperty(kAudioUnitProperty_PresentPreset) failed: %i", err);
		return;
	}
	
	id classInfoPlist = NULL;
	UInt32 dataSize = sizeof(classInfoPlist);
	
	err = AudioUnitGetProperty([self audioUnit],
							   kAudioUnitProperty_ClassInfo, 
							   kAudioUnitScope_Global, 
							   0, 
							   &classInfoPlist, 
							   &dataSize);
	if (noErr != err) {
		NSLog(@"SoftSynthPlugin: AudioUnitGetProperty(kAudioUnitProperty_ClassInfo) failed: %i", err);
		return;
	}
	
	NSString *errorString = nil;
	NSData *xmlData = [NSPropertyListSerialization dataFromPropertyList:classInfoPlist format:NSPropertyListXMLFormat_v1_0 errorDescription:&errorString];
	
	if (nil == xmlData) {
		NSLog(@"SoftSynthPlugin: Unable to create property list from AU class info: %@", errorString);
		[errorString release];
		return;
	}
	
	// Create the directory structure if required
	NSString *presetPath = [[presetURL path] stringByDeletingLastPathComponent];
	// [NSFileManager defaultManager] is NOT thread safe
	NSFileManager *fileManager = [[NSFileManager alloc] init];	//instance is thread safe.
	if (![fileManager fileExistsAtPath:presetPath]) {
		NSError *error = nil;
		NSMutableDictionary *attributes = nil;
		NSLog(@"pathComponents: %@", [presetPath pathComponents]);
		if ([[[presetPath pathComponents] second] isEqualToString:@"Library"]) {
			//		NSMutableDictionary *attributes = [NSMutableDictionary dictionaryWithObjectsAndKeys:
			//								[NSNumber numberWithInt:0], NSFileGroupOwnerAccountID,
			//								[NSNumber numberWithInt:0], NSFileOwnerAccountID,
			//								@"root", NSFileGroupOwnerAccountName,
			//								@"root", NSFileOwnerAccountName, nil ];
			attributes = [NSMutableDictionary dictionary];
			//		[attributes setObject:@"root" forKey:NSFileOwnerAccountID];
			//		[attributes setObject:@"admin" forKey:NSFileGroupOwnerAccountName];
			[attributes setObject:[NSNumber numberWithInt:511] forKey:NSFilePosixPermissions];
			[fileManager setAttributes:attributes ofItemAtPath:presetPath error:&error];
			if (error){
				NSLog(@"Error settings permission %@",[error description]);
			}		
		}
		BOOL dirStructureCreated = [fileManager createDirectoryAtPath:presetPath 
										  withIntermediateDirectories:YES 
														   attributes:attributes 
																error:&error];
		if (!dirStructureCreated) {
			NSLog(@"SoftSynthPlugin: Unable to create directories for %@\n%@", presetURL, error);
			NSString *messageText = [NSString stringWithFormat:@"SoftSynthPlugin: Unable to create directories for %@", presetURL];
			NSString *informativeText = [error description];
//			NSAlert *alert = [self inform:messageText informativeText:[error description]];
//			[alert beginSheetModalForWindow:[self window] 
//							  modalDelegate:self 
////							 didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
//							 didEndSelector:nil
//								contextInfo:nil];
			inform([messageText UTF8String], [informativeText UTF8String]);
			[messageText release];
			[informativeText release];
			return;
		}
	}
	[fileManager release];
	
	BOOL presetSaved = [xmlData writeToURL:presetURL atomically:YES];
	if (!presetSaved) {
		NSLog(@"SoftSynthPlugin: Unable to save preset to %@", presetURL);
		return;
	}
	
	[self notifyAUParameterListenerOfParameterChanges:[self audioUnit]];
}

- (NSURL *)presetDomainURL:(FSVolumeRefNum)domain
{
	FSRef presetFolderRef;
	OSErr result = FSFindFolder(domain, kAudioPresetsFolderType, kDontCreateFolder, &presetFolderRef);
	if (result != noErr) {
		NSLog(@"PluginUIController: FSFindFolder(kAudioPresetsFolderType) failed: %i", result);
		return nil;
	}
	CFURLRef presetsFolderURL = CFURLCreateFromFSRef(kCFAllocatorSystemDefault, &presetFolderRef);
	if (presetsFolderURL == nil) {
		NSLog(@"PluginUIController: CFURLCreateFromFSRef failed");	
		return nil;
	}
	NSArray *ManufacturerAndName = [[self class] getManufacturerAndNameFromAudioUnit:[self audioUnit]];
	NSString *auManufacturer = [ManufacturerAndName first];
	NSString *auName = [ManufacturerAndName second];
	NSArray *pathComponents = [NSArray arrayWithObjects:[(NSURL *)presetsFolderURL path], auManufacturer, auName, nil];
	NSURL *url = [NSURL fileURLWithPathComponents:pathComponents];
	NSLog(@"savePreset: path: %@", [url path]);
	return url;
}

- (NSPopUpButton *)getViewFrom:(id)aView named:(NSString *)className
{
	NSPopUpButton *popup = nil;
	if ([NSStringFromClass([aView class]) isEqual:className]) {
		popup = aView;
	} else if (([NSStringFromClass([aView class]) isEqual:@"NSView"]) || 
			   ([aView isKindOfClass:[NSView class]])) {
		NSArray *subviews = [aView subviews];
		id view;
		for (view in subviews) {
			if ([NSStringFromClass([view class]) isEqual:className]) {
				popup = view;
				break;
			}
		}
	}
	return popup;
}
- (NSPopUpButton *)getPopUpButtonFrom:(id)aView
{
	return [self getViewFrom: aView named:@"NSPopUpButton"];
}

- (void)setSelectPresetPicked
{
	NSPopUpButton *popup = [self getPopUpButtonFrom:[self presetPopUpView]];
	if (popup) {
		[popup selectItemAtIndex:[self presetPicked]];
	}
}

- (FSVolumeRefNum)decideDomain:(id)sender
{
	NSString *senderTitle = nil;
	if ([NSStringFromClass([sender class]) isEqual:@"NSMenuItem"]) {
		// for ordinary NSMenus, the title is item #0, so we have to offset things
		senderTitle = [sender title];
		//		itemIndex = [[sender menu] indexOfItem:sender];
	} else {
		// this is an NSPopUpButton, so the first useful item really is #0
		senderTitle = [sender title];
		//		itemIndex = [sender indexOfSelectedItem];
	}
	NSLog(@"savePreset: %@ %@", sender, senderTitle);
	FSVolumeRefNum domain;
	if ([[senderTitle lowercaseString] containsString:@"local" ]) {
		domain = kLocalDomain;
		NSLog(@"kLocalDomain");
	} else {
		domain = kUserDomain;
		NSLog(@"kUserDomain");
	}
	return domain;
}

- (IBAction) savePresetToFile:(id)sender
{
#pragma unused(sender)
	NSSavePanel *savePanel = [NSSavePanel savePanel];
	NSString *initialFileName = [self currentPresetFileName];
	NSLog(@"savePresetToFile: %@", initialFileName);
	
	[savePanel setAllowedFileTypes:[NSArray arrayWithObject:presetObject]];
	[savePanel setNameFieldStringValue:initialFileName];
	
	[savePanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result){
		if(NSFileHandlingPanelOKButton == result)
			[self saveCustomPresetToURL:[savePanel URL] presetName:nil];
	}];
}

- (NSDictionary *)detectPresetDictionary:(NSString *)presetName domain:(NSString *)domain
{
	// detect presetsDictionary by domain
	NSDictionary *presetsDict = nil;
	for (NSDictionary *dict in [self presetsTree]) {
		NSString *domainValue = [dict objectForKey:@"presetName"];
		if ([domainValue isEqualToString:domain]) {
			presetsDict = dict;
			break;
		}
	}
	if (presetsDict == nil) {
		return nil;
	}
	// detect presetDictionary by presetName
	NSDictionary *presetDict = nil;
	NSArray *presets = [presetDict objectForKey:@"children"];
	for (NSDictionary *preset in presets) {
		NSString *presetNameValue = [preset objectForKey:@"presetName"];
		if ([presetNameValue isEqualToString:presetName]) {
			NSNumber *presetNumber = [preset objectForKey:@"presetNumber"];
			NSString *presetPath = [preset objectForKey:@"presetPath"];
			NSLog(@"loadUserPreset: %@ %@", presetNumber, presetPath);
			if ([presetNumber intValue] == -1) {
				[self loadCustomPresetFromURL:[NSURL fileURLWithPath:presetPath]];
				break;
			}
		}
	}
	return presetDict;
}

- (IBAction) loadPresetFromFile:(id)sender
{
#pragma unused(sender)
	
	NSOpenPanel *openPanel = [NSOpenPanel openPanel];
	
	[openPanel setCanChooseFiles:YES];
	[openPanel setCanChooseDirectories:NO];
	[openPanel setAllowsMultipleSelection:NO];
	[openPanel setAllowedFileTypes:[NSArray arrayWithObject:presetObject]];
	
	[openPanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result){
		if(NSFileHandlingPanelOKButton == result)
			[self loadCustomPresetFromURL:[[openPanel URLs] lastObject]];
	}];
}

- (void)loadDomainPreset:(NSString *)presetName domain:(NSString *)domain
{
	NSDictionary *preset = [self detectPresetDictionary:presetName domain:domain];
	if (preset == nil) {
		return;
	}
	NSNumber *presetNumber = [preset objectForKey:@"presetNumber"];
	NSString *presetPath = [preset objectForKey:@"presetPath"];
	NSLog(@"presetNumber=%@ presetPath=%@", presetNumber, presetPath);
	if ([presetNumber intValue] == -1) {
		[self loadCustomPresetFromURL:[NSURL fileURLWithPath:presetPath]];
	}
}

- (void)loadUserPreset:(id)sender
{
//	NSString *presetName = [sender title];
//	NSDictionary *preset = [self detectPresetDictionary:presetName domain:UserDomain];
//	if (preset == nil) {
//		return;
//	}
//	NSNumber *presetNumber = [preset objectForKey:@"presetNumber"];
//	NSString *presetPath = [preset objectForKey:@"presetPath"];
//	if ([presetNumber intValue] == -1) {
//		[self loadCustomPresetFromURL:[NSURL fileURLWithPath:presetPath]];
//	}
	[self loadDomainPreset:[sender title] domain:UserDomain];
	

//	NSDictionary *presetsDict = nil;
//	for (NSDictionary *dict in [self presetsTree]) {
//		NSString *value = [dict objectForKey:@"presetName"];
//		if ([value isEqualToString:UserDomain]) {
//			presetsDict = dict;
//			break;
//		}
//	}
//	NSArray *presets = [presetsDict objectForKey:@"children"];
//	for (NSDictionary *dict in presets) {
//		NSString *value = [dict objectForKey:@"presetName"];
//		if ([value isEqualToString:presetName]) {
//			NSNumber *presetNumber = [dict objectForKey:@"presetNumber"];
//			NSString *presetPath = [dict objectForKey:@"presetPath"];
//			NSLog(@"loadUserPreset: %@ %@", presetNumber, presetPath);
//			if ([presetNumber intValue] == -1) {
//				[self loadCustomPresetFromURL:[NSURL fileURLWithPath:presetPath]];
//			}
//			break;
//		}
//	}
	
}

- (void)loadLocalPreset:(id)sender
{
	NSLog(@"loadLocalPreset: called from %@", sender);
	[self loadDomainPreset:[sender title] domain:LocalDomain];
}

- (IBAction)loadPreset:(id)sender
{
#pragma unused(sender)
	[self setSelectPresetPicked];
	NSOpenPanel *openPanel = [NSOpenPanel openPanel];
	FSVolumeRefNum domain = [self decideDomain:sender];
	NSURL *presetURL = [self presetDomainURL:domain];
	
	[openPanel setCanChooseFiles:YES];
	[openPanel setCanChooseDirectories:NO];
	[openPanel setAllowsMultipleSelection:NO];
	[openPanel setAllowedFileTypes:[NSArray arrayWithObject:presetObject]];
	[openPanel setDirectoryURL:presetURL];
	
	[openPanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result){
		if (NSFileHandlingPanelOKButton == result) {
			[self loadCustomPresetFromURL:[[openPanel URLs] lastObject]];
		}
	}];
//	NSLog(@"presetPopUpView subviews: %@", [presetPopUpView subviews]);
//	[[[presetPopUpView subviews] first] selectItemAtIndex:presetPicked];
//	[popup selectItemAtIndex:presetPicked];
	[self notifyAUParameterListenerOfParameterChanges:[self audioUnit]];
}

#pragma mark TODO
- (NSString *)currentPresetFileName
{
	NSLog(@"currentPresetFileName1");
//	NSString *audioUnitBasename = [[self class] getAudioUnitBasename:[self audioUnit]];
//	NSString *currentPresetName = [self getCurrentPresetName:[self audioUnit]];
	NSString *initialFileName = [NSString stringWithFormat:@"%@(%@)", 
								 [self getAudioUnitBasename:[self audioUnit]], 
								 [self getCurrentPresetName:[self audioUnit]]];
	NSLog(@"currentPresetFileName2");
	NSString *currentPresetName = [self getCurrentPresetName:[self audioUnit]];
	NSLog(@"currentPresetFileName3");
	return currentPresetName;
}

- (void)savePresetOn:(FSVolumeRefNum)domain
{
	NSString *initialFileName = [self currentPresetFileName];
	
	NSURL *presetURL = [self presetDomainURL:domain];
	//	NSString *presetDir = [[presetURL path] stringByDeletingLastPathComponent];
	NSString *presetDir = [presetURL path];
	NSLog(@"savePreset: presetDir: %@", presetDir);
	NSFileManager *fileManager = [[NSFileManager alloc] init];	//instance is thread safe.
	if (![fileManager fileExistsAtPath:presetDir]) {
		NSLog(@"not fileExistsAtPath: %@", presetDir);
		NSError *error = nil;
		BOOL dirStructureCreated = [fileManager createDirectoryAtPath:presetDir 
										  withIntermediateDirectories:YES 
														   attributes:nil 
																error:&error];
		if (!dirStructureCreated) {
			NSLog(@"SoftSynthPlugin: Unable to create directories for %@ (%@)", presetURL, error);
			NSAlert *alert = [self confirm:@"save preset file failed.\nplease use user domain or custom location."
						   informativeText:[error description]];
			[alert beginSheetModalForWindow:[self window] 
							  modalDelegate:self 
							 didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
								contextInfo:alert];
			NSLog(@"after beginSheetModalForWindow:");
			return;
		}
	}

	NSSavePanel *savePanel = [NSSavePanel savePanel];
	
	[savePanel setAllowedFileTypes:[NSArray arrayWithObject:presetObject]];
	[savePanel setDirectoryURL:presetURL];
	[savePanel setNameFieldStringValue:initialFileName];
	
	[savePanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result) {
		if (NSFileHandlingPanelOKButton == result) {
			[self saveCustomPresetToURL:[savePanel URL] presetName:nil];
		}
	}];
}

#pragma mark TODO
- (NSArray *)presetsForDomain:(short)domain
{
	FSRef presetFolderRef;
	OSErr err = FSFindFolder(domain, kAudioPresetsFolderType, kDontCreateFolder, &presetFolderRef);
	if (err != noErr) {
		NSLog(@"SoftSynthPlugin: FSFindFolder(kAudioPresetsFolderType) failed: %i", err);	
		return nil;
	}
	
	CFURLRef presetsFolderURL = CFURLCreateFromFSRef(kCFAllocatorSystemDefault, &presetFolderRef);
	if (presetsFolderURL == nil) {
		NSLog(@"SoftSynthPlugin: CFURLCreateFromFSRef failed");	
		return nil;
	}

	NSArray *manufacturerAndName = [self getManufacturerAndNameFromAudioUnit:[self audioUnit]];
	NSString *auManufacturer = [manufacturerAndName first];
	NSString *auName = [manufacturerAndName last];
	NSArray *pathComponents = [NSArray arrayWithObjects:[(NSURL *)presetsFolderURL path], auManufacturer, auName, nil];
	NSString *auPresetsPath = [[NSString pathWithComponents:pathComponents] stringByStandardizingPath];
	
	NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:auPresetsPath];
	NSString *path = nil;
	NSMutableArray *result = [[NSMutableArray alloc] init];
	
	while ((path = [enumerator nextObject])) {
		// Skip files that aren't AU presets
		if ([[path pathExtension] isEqualToString:presetObject] == NO)
			continue;
		
		NSNumber *presetNumber = [NSNumber numberWithInt:-1];
		NSString *presetName = [[path lastPathComponent] stringByDeletingPathExtension];
		NSString *presetPath = [auPresetsPath stringByAppendingPathComponent:path];
		
		[result addObject:[NSDictionary dictionaryWithObjectsAndKeys:presetNumber, @"presetNumber", presetName, @"presetName", presetPath, @"presetPath", nil]];
	}
	
	CFRelease(presetsFolderURL), presetsFolderURL = nil;
	
	NSSortDescriptor *sortDescriptor = [[[NSSortDescriptor alloc] initWithKey:@"presetName" ascending:YES] autorelease];
	[result sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
	
	return [result autorelease];
}

- (NSArray *)localPresets
{
	return [self presetsForDomain:kLocalDomain];
}

- (NSArray *)userPresets
{
	return [self presetsForDomain:kUserDomain];
}

- (void)scanPresets
{
//	NSMutableArray *_presetsTree = [[NSMutableArray alloc] init];
	
	NSArray *factoryPresets = nil;
	UInt32 dataSize = sizeof(factoryPresets);

	ComponentResult err = AudioUnitGetProperty([self audioUnit], 
											   kAudioUnitProperty_FactoryPresets,
											   kAudioUnitScope_Global, 
											   0, 
											   &factoryPresets, 
											   &dataSize);
	// Delay error checking
	[[self presetsTree] removeAllObjects];
	NSMutableArray *factoryPresetsArray = [NSMutableArray array];
	if (err == noErr) {
		for (NSUInteger i = 0; i < [factoryPresets count]; ++i) {
			AUPreset *preset = (AUPreset *)[factoryPresets objectAtIndex:i];
			NSNumber *presetNumber = [NSNumber numberWithInt:preset->presetNumber];
			NSString *presetName = [(NSString *)preset->presetName copy];
			
			[factoryPresetsArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:presetNumber, @"presetNumber", presetName, @"presetName", nil]];
			[presetName release];
		}
		NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"presetName" ascending:YES];
		[factoryPresetsArray sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
		[sortDescriptor release];
	} else {
		NSLog(@"SoftSynthPlugin: AudioUnitGetProperty(kAudioUnitProperty_FactoryPresets) failed: %i", err);
	}
	
	if ([factoryPresetsArray count]) {
		[[self presetsTree] addObject:[NSDictionary dictionaryWithObjectsAndKeys:factoryPresetsArray, @"children", NSLocalizedString(FactoryDomain, @""), @"presetName", nil]];
	}
	
	NSArray *localPresetsArray = [self localPresets];
	if ([localPresetsArray count]) {
		[[self presetsTree] addObject:[NSDictionary dictionaryWithObjectsAndKeys:localPresetsArray, @"children", NSLocalizedString(LocalDomain, @""), @"presetName", nil]];
	}

	NSArray *userPresetsArray = [self userPresets];
	if ([userPresetsArray count]) {
		[[self presetsTree] addObject:[NSDictionary dictionaryWithObjectsAndKeys:userPresetsArray, @"children", NSLocalizedString(UserDomain, @""), @"presetName", nil]];
	}
//	[_presetsOutlineView reloadData];
	
	[factoryPresets release];
}

- (void) savePresetDidEnd:(NSAlert *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
	// sheet is contextInfo
	NSAlert *request = (NSAlert *)contextInfo;
	NSString *presetFileName = [[self getViewFrom:[sheet accessoryView] 
											  named:@"NSTextField"] 
								  stringValue];
	NSString *domainSelection = [[[self getViewFrom:[sheet accessoryView] 
											  named:@"NSPopUpButton"] 
								  selectedItem] title];
	[request autorelease];
	NSLog(@"savePresetDidEnd: %@", domainSelection);
	FSVolumeRefNum domain = kUserDomain;
	if (domainSelection) {
		if ([domainSelection isEqualToString:UserDomain]) {
			domain = kUserDomain;
		} else if ([domainSelection isEqualToString:LocalDomain]) {
			domain = kLocalDomain;
		}
	}
	if (!(returnCode == NSAlertFirstButtonReturn || returnCode == NSOKButton)) {
		return;
	}
	NSLog(@"savePresetDidEnd:");
	FSRef presetFolderRef;
	OSErr err = FSFindFolder(domain, kAudioPresetsFolderType, kDontCreateFolder, &presetFolderRef);
	if (err != noErr) {
		NSLog(@"SoftSynthPlugin: FSFindFolder(kAudioPresetsFolderType) failed: %i", err);
	}
	CFURLRef presetsFolderURL = CFURLCreateFromFSRef(kCFAllocatorSystemDefault, &presetFolderRef);
	if (presetsFolderURL == nil) {
		NSLog(@"SoftSynthPlugin: CFURLCreateFromFSRef failed.");
		return;
	}
	NSArray *manufacturerAndName = [self getManufacturerAndNameFromAudioUnit:[self audioUnit]];
	NSString *auManufacturer = [manufacturerAndName first];
	NSString *auName = [manufacturerAndName last];
	NSArray *pathComponents = [NSArray arrayWithObjects:[(NSURL *)presetsFolderURL path], auManufacturer, auName, presetFileName, nil];
	NSString *audioUnitPresetPath = [[[NSString pathWithComponents:pathComponents] stringByAppendingPathExtension:presetObject] stringByStandardizingPath];
	[self saveCustomPresetToURL:[NSURL fileURLWithPath:audioUnitPresetPath] presetName:presetFileName];
	[self scanPresets];
	
	CFRelease(presetsFolderURL);
	presetsFolderURL = nil;
}

- (IBAction)savePreset:(id)sender
{
	[self setSelectPresetPicked];
//	NSString *senderTitle = nil;
//	if ([NSStringFromClass([sender class]) isEqual:@"NSMenuItem"]) {
//		// for ordinary NSMenus, the title is item #0, so we have to offset things
//		senderTitle = [sender title];
////		itemIndex = [[sender menu] indexOfItem:sender];
//	} else {
//		// this is an NSPopUpButton, so the first useful item really is #0
//		senderTitle = [sender title];
////		itemIndex = [sender indexOfSelectedItem];
//	}
//	NSLog(@"savePreset: %@ %@", sender, senderTitle);
//	FSVolumeRefNum domain;
//	if ([[senderTitle lowercaseString] containsString:@"local" ]) {
//		domain = kLocalDomain;
//		NSLog(@"kLocalDomain");
//	} else {
//		domain = kUserDomain;
//		NSLog(@"kUserDomain");
//	}
	if (true) {
		NSAlert *request = [self requestDomain:@"save preset..." 
							   informativeText:@"input preset name and select location."
								   initialText:[self currentPresetFileName]];
		[request beginSheetModalForWindow:[self window] 
						  modalDelegate:self 
						 didEndSelector:@selector(savePresetDidEnd:returnCode:contextInfo:)
							contextInfo:request];
		return;
	}
	
	FSVolumeRefNum domain = [self decideDomain:sender];
	[self savePresetOn:domain];

//	if (YES) {
//		[self savePresetOn:domain];
//		return;
//	}
//	
//	NSString *currentPresetName = [self getCurrentPresetName:[self audioUnit]];
//	NSString *audioUnitBasename = [[self class] getAudioUnitBasename:[self audioUnit]];
//	NSString *initialFileName = [NSString stringWithFormat:@"%@(%@)", (NSString *) audioUnitBasename, (NSString *) currentPresetName];
//	
//	NSURL *presetURL = [self presetDomainURL:domain];
//	NSLog(@"savePreset: path: %@", [presetURL path]);
////	NSString *presetDir = [[presetURL path] stringByDeletingLastPathComponent];
//	NSString *presetDir = [presetURL path];
//	NSLog(@"savePreset: presetDir: %@", presetDir);
//	if (![[NSFileManager defaultManager] fileExistsAtPath:presetDir]) {
//		NSLog(@"not fileExistsAtPath: %@", presetDir);
//		NSError *error = nil;
//		NSFileManager *fileManager = [[NSFileManager alloc] init];	//instance is thread safe.
//		BOOL dirStructureCreated = [fileManager createDirectoryAtPath:presetDir withIntermediateDirectories:YES attributes:nil error:&error];
//		if (!dirStructureCreated) {
//			NSLog(@"SoftSynthPlugin: Unable to create directories for %@ (%@)", presetURL, error);
////			
////			NSAlert *alert = [NSAlert alertWithMessageText: @"save preset file failed.\nMove to user domain?"
////											 defaultButton: @"OK" 
////										   alternateButton: @"Cancel" 
////											   otherButton: nil
////								 informativeTextWithFormat: [error description] ];
////			int result = [alert runModal];
////			if (result == NSAlertAlternateReturn) {
////				return;
////			}
//			int response;
//			NSAlert *alert = 
//			[self confirm:@"save preset file failed.\nplease use user domain or custom location." informativeText:[error description]];
//			[alert beginSheetModalForWindow:[self window] 
//			 					  modalDelegate:self 
//			 					 didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
//			 contextInfo:nil];
////		contextInfo:&response];
//			NSLog(@"after beginSheetModalForWindow:");
//			return;
//		}
//	}
//
//	NSSavePanel *savePanel = [NSSavePanel savePanel];
//	
//	[savePanel setAllowedFileTypes:[NSArray arrayWithObject:presetObject]];
//	[savePanel setDirectoryURL:presetURL];
//	[savePanel setNameFieldStringValue:initialFileName];
//	
//	[savePanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger result) {
//		if (NSFileHandlingPanelOKButton == result) {
//			[self saveCustomPresetToURL:[savePanel URL] presetName:nil];
//		}
//	}];
}

- (void)alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo
{
	//	*contextInfo = returnCode;
	//	NSLog(@"alertDidEnd: %@ %d %d", alert, returnCode, *contextInfo);
	NSLog(@"alertDidEnd: %@ %d %@", alert, returnCode, (NSAlert *)contextInfo);
	//dismiss the sheet from within the didEndSelector method 
	//	[[alert window] orderOut: nil];
//	[[self window] orderOut: [alert window]];
	switch (returnCode) {
		case NSAlertFirstButtonReturn:
			NSLog(@"alertDidEnd: %d", returnCode);
			break;
		case NSAlertSecondButtonReturn:
			NSLog(@"alertDidEnd: %d", returnCode);
			break;
		case NSAlertThirdButtonReturn:
			NSLog(@"alertDidEnd: %d", returnCode);
			break;
		default:
			break;
	}
	if (returnCode == NSAlertFirstButtonReturn) {
		NSLog(@"alertDidEnd");
//		[self savePresetOn:kUserDomain];
	}
	//	if(returnCode == NSAlertDefaultReturn) {
	//		NSLog(@"NSAlertDefaultReturn");
	//	}
	//	else if(returnCode == NSAlertAlternateReturn) {
	//		NSLog(@"NSAlertAlternateReturn");
	//	}
	//	else if(returnCode == NSAlertOtherReturn) {
	//		NSLog(@"NSAlertOtherReturn");
	//	}
	//	else if(returnCode == NSAlertErrorReturn) {
	//		NSLog(@"NSAlertErrorReturn");
	//	}
}


#pragma mark -
#pragma mark Toolbar
- (void) setupToolbar
{
	NSLog(@"setupToolbar has been called");
//	toolbar = [[[NSToolbar alloc] initWithIdentifier:@"AudioUnitViewToolBar"] autorelease];
//	viewStylePopUpView = [[[NSView alloc] initWithFrame: [[self window] frame]] autorelease];
	[self setToolbar:[[[NSToolbar alloc] initWithIdentifier:@"AudioUnitViewToolBar"] autorelease]];
	[self setViewStylePopUpView:[[[NSView alloc] initWithFrame: [[self window] frame]] autorelease]];
	
	// configure our toolbar (note: this can also be done in Interface Builder)
	
	// If you pass NO here, you turn off the customization palette.  The palette is normally handled
	// automatically for you by NSWindow's -runToolbarCustomizationPalette: method; you'll notice
	// that the "Customize Toolbar" menu item is hooked up to that method in Interface Builder.
	// Interface Builder currently doesn't automatically show this action (or the -toggleToolbarShown: action)
	// for First Responder/NSWindow (this is a bug), so you have to manually add those methods to the
	// First Responder in Interface Builder (by hitting return on the First Responder and adding the
	// new actions in the usual way) if you want to wire up menus to them.
	//
	[[self toolbar] setAllowsUserCustomization:YES];
    
	// tell the toolbar that it should save any configuration changes to user defaults.  ie. mode
	// changes, or reordering will persist.  Specifically they will be written in the app domain using
	// the toolbar identifier as the key. 
	//
	[[self toolbar] setAutosavesConfiguration:YES];
	
	// tell the toolbar to show icons only by default
	//[toolbar setDisplayMode:NSToolbarDisplayModeIconOnly];
	// tell the toolbar to show label only by default
	//[toolbar setDisplayMode:NSToolbarDisplayModeLabelOnly];
	[[self toolbar] setDisplayMode:NSToolbarDisplayModeIconAndLabel];
	//[toolbar setDisplayMode:NSToolbarDisplayModeDefault];
	
	// this is a state variable that keeps track of whether we're set to Cocoa, Carbon or Generic
//	viewStylePicked = 1;
	[self setViewStylePicked:1];
	
//	viewStyleMenu = [[[NSMenu alloc] initWithTitle: @"View Style"] autorelease];
//	presetMenu = [[[NSMenu alloc] initWithTitle: @"Preset"] autorelease];
	[self setViewStyleMenu:[[[NSMenu alloc] initWithTitle: @"View Style"] autorelease]];
	[self setPresetMenu:[[[NSMenu alloc] initWithTitle: @"Preset"] autorelease]];
	[[self presetMenu] setShowsStateColumn: YES];
	[[self presetMenu] setAutoenablesItems: YES];
	[[self toolbar] setDelegate: self];
	[[self window] setToolbar:[self toolbar]];
	[[self toolbar] release];
}


#pragma mark -
#pragma mark Menu Item Validation
//--------------------------------------------------------------------------------------------------
// Update or validate our menu items
//--------------------------------------------------------------------------------------------------
- (BOOL)validateMenuItem:(NSMenuItem *)menuItem
{
//	NSLog(@"validateMenuItem: has been called");
	// find out which menu item we are attempting to update by examining it's action selector
	//
	if ([menuItem action] == @selector(changePreset:)) {
		// update the menu item's checkmark state if the menu matches are tracked chosen menu item
		if ([menuItem tag] == [self presetPicked]) {
			[menuItem setState:NSOnState];
		} else {
			[menuItem setState:NSOffState];
		}
	} else if (([menuItem action] == @selector(changeViewStyle:))) {
		if ([menuItem tag] == [self viewStylePicked]) {
			[menuItem setState:NSOnState];
		} else {
			[menuItem setState:NSOffState];
		}
	} else {
		[menuItem setState:NSOffState];
		if ([menuItem action] == @selector(loadPreset:)) {
			NSLog(@"loadPreset: %@ %d", menuItem, [self presetPicked]);
		} else if ([menuItem action] == @selector(savePreset:)) {
			NSLog(@"savePreset: %@ %d", menuItem, [self presetPicked]);
		}
	}

	return YES;
}

#pragma mark -
#pragma mark NSToolbarItemValidation
//--------------------------------------------------------------------------------------------------
// We don't do anything useful here (and we don't really have to implement this method) but you could
// if you wanted to. If, however, you want to have validation checks on your standard NSToolbarItems
// (with images) and have inactive ones grayed out, then this is the method for you.
// It isn't called for custom NSToolbarItems (with custom views); you'd have to override -validate:
// (see NSToolbarItem.h for a discussion) to get it to do so if you wanted it to.
// If you don't implement this method, the NSToolbarItems are enabled by default.
//--------------------------------------------------------------------------------------------------
- (BOOL)validateToolbarItem:(NSToolbarItem *)theItem
{
//	NSLog(@"validateToolbarItem: has been called");
	// You could check [theItem itemIdentifier] here and take appropriate action if you wanted to
	return YES;
}

#pragma mark -
#pragma mark NSToolbarDelegate

/*
 The following three methods are required for toolbars that are not created in Interface Builder.  
 If the toolbar is created in IB, you may omit them.  If you do implement them, any  items returned 
 by the delegate will be used alongside items created in Interface Builder.
 */

/* 
 Given an item identifier, this method returns an item.  
 Note that, it is expected that each toolbar receives its own distinct copies.   
 If the item has a custom view, that view should be in place when the item is returned.  
 Finally, do not assume the returned item is going to be added as an active item in the toolbar.  
 In fact, the toolbar may ask for items here in order to construct the customization palette 
 (it makes copies of the returned items).  if willBeInsertedIntoToolbar is YES, 
 the returned item will be inserted, and you can expect toolbarWillAddItem: is about to be posted.
 */
//--------------------------------------------------------------------------------------------------
// This method is required of NSToolbar delegates.
// It takes an identifier, and returns the matching NSToolbarItem. It also takes a parameter telling
// whether this toolbar item is going into an actual toolbar, or whether it's going to be displayed
// in a customization palette.
//--------------------------------------------------------------------------------------------------
- (NSToolbarItem *)toolbar:(NSToolbar *)toolbar
	 itemForItemIdentifier:(NSString*)identifier
 willBeInsertedIntoToolbar:(BOOL)willBeInserted 
{
    NSToolbarItem *toolbarItem = nil;
	
	NSLog(@"toolbar:itemForItemIdentifier:willBeInsertedIntoToolbar: \
		  has been called");
	if (FALSE) {
		NSPopUpButton *popup = [[NSPopUpButton alloc] initWithFrame: 
								NSMakeRect(0, 0, 150, 22)];
		BOOL isNSView = [popup isKindOfClass:[NSView class]];
		if (isNSView) {
			NSLog(@"NSPopUpButton is kind of class: NSView");
		} else {
			NSLog(@"NSPopUpButton is NOT kind of class: NSView");
		}		
		return [toolbarItem autorelease];;
	}
	
	if ([identifier isEqual: @"First"]) {
		[toolbarItem setLabel: @"First item"];
		[toolbarItem setImage: [NSImage imageNamed: @"FileIcon_Directory"]];
	} else if ([identifier isEqual:@"Second"]) {
		[toolbarItem setLabel: @"Second item"];
		[toolbarItem setImage: [NSImage imageNamed: @"SecondImage"]];
		[toolbarItem setTarget: nil];
		[toolbarItem setAction: @selector(toggleToolbarShown:)];
	} else if ([identifier isEqual:@"Popup"]) {
	} else if ([identifier isEqual:kViewStyleToolbarItemID]) {
		toolbarItem = [[NSToolbarItem alloc]
					   initWithItemIdentifier: identifier];
		NSPopUpButton *popup = [[NSPopUpButton alloc] initWithFrame: 
								NSMakeRect(0, 0, 150, 22)];
		[[popup cell] setBezeled:YES];
 		[[popup cell] setControlSize:NSSmallControlSize];
		
		[popup setAutoenablesItems: NO];
		[popup setAction: @selector(changeViewStyle:)];
		
		[popup addItemWithTitle: @"Marguerite"];
		[[popup itemWithTitle: @"Marguerite"] setEnabled: YES];
		[popup addItemWithTitle: @"Julie"];
		[[popup itemWithTitle: @"Julie"] setEnabled: YES];
		[popup addItemWithTitle: @"Liv"];
		[[popup itemWithTitle: @"Liv"] setEnabled: YES];
		[popup addItemWithTitle: @"Juliette"];
		[[popup itemWithTitle: @"Juliette"] setEnabled: YES];
		
		id item = [popup itemWithTitle: @"Juliette"];
		[popup selectItem: item];
		
		[toolbarItem setLabel: @"View Style"];
		[toolbarItem setView: popup];
		[toolbarItem setTarget: self];
		//		NSView *view = [[NSView alloc] initWithFrame:[popup frame]];
		//				[view addSubview:popup];
		//viewStylePopUpView = [[NSView alloc] initWithFrame:[popup frame]];
		[self setViewStylePopUpView:[[NSView alloc] initWithFrame:[popup frame]]];
		[[self viewStylePopUpView] addSubview:popup];
		//		[viewStylePopUpView];
		[toolbarItem setView:[self viewStylePopUpView]];
		
		//		[toolbarItem setMinSize: NSMakeSize(100, 100)];
		//		[toolbarItem setMaxSize: NSMakeSize(100, 100)];
		[toolbarItem setMinSize: [self viewStylePopUpView].frame.size];
		[toolbarItem setMaxSize: [self viewStylePopUpView].frame.size];
		//		[toolbarItem setMinSize:[toolbarView frame].size];
		//		[toolbarItem setMaxSize:[toolbarView frame].size];
		//		return [toolbarItem autorelease];
		
		//		[popup setAutoenablesItems: NO];  // item state set by menuNeedsUpdate:
		//		[[popup menu] setDelegate: self];
		//        NSSize minimumSize = [popup bounds].size;
		//        NSSize maximumSize = [popup bounds].size;
		//        [toolbarItem setMinSize: minimumSize];
		//        [toolbarItem setMaxSize: maximumSize];
		[[popup cell] setControlSize: NSSmallControlSize];
		
		// for text only
		NSMenuItem *menuItem = [[[NSMenuItem alloc] initWithTitle: @"View Style" 
														   action: NULL
													keyEquivalent: @""]
								autorelease];
		//		submenu = [[[NSMenu alloc] initWithTitle: @""] autorelease];
		//		NSMenu *submenu = viewStyleMenu;
		NSMenu *viewStyleMenu = [self viewStyleMenu];
		[viewStyleMenu addItemWithTitle: @"Marguerite" 
								 action: @selector(changeViewStyle:) 
						  keyEquivalent: @""];
		[viewStyleMenu addItemWithTitle: @"Julie" 
								 action: @selector(changeViewStyle:) 
						  keyEquivalent: @""];
		[viewStyleMenu addItemWithTitle: @"Liv" 
								 action: @selector(changeViewStyle:) 
						  keyEquivalent: @""];
		[viewStyleMenu addItemWithTitle: @"Juliette" 
								 action: @selector(changeViewStyle:) 
						  keyEquivalent: @""];
		[menuItem setSubmenu: viewStyleMenu];
		[toolbarItem setMenuFormRepresentation: menuItem];
		//		[toolbarItem setView: popup];
		//return toolbarItem;
	} else if ([identifier isEqual:kPresetToolbarItemID]) {
		// 2) preset toolbar item
		//		toolbarItem = [[NSToolbarItem alloc]
		//					   initWithItemIdentifier: identifier];
		NSPopUpButton *popup = [[NSPopUpButton alloc] initWithFrame:NSMakeRect(0, 0, 150, 22)];
		[[popup cell] setBezeled:YES];
		[[popup cell] setControlSize:NSSmallControlSize];
		
		[popup setAutoenablesItems: NO];
		//		[popup setAction: @selector(changePreset:)];
		
		NSMenuItem *menuItem = [[[NSMenuItem alloc] initWithTitle: @"Preset"
														   action: NULL
													keyEquivalent: @""]
								autorelease];
		
		
		// get current preset
		CFArrayRef presets = [[self class] getPresets:[self audioUnit]];
		//		[popup setPullsDown: YES];
		//		BOOL hasPresets = presets != NULL;
		//		[popup setEnabled:hasPresets];
		//		[menuItem setEnabled:hasPresets];
		//		NSMenuItem *item = nil;
		if (presets != NULL) {
			AUPreset currentPreset = [self getCurrentPreset:[self audioUnit]];
			NSLog(@"%x", currentPreset);
			int count = CFArrayGetCount(presets);
			for (int i = 0; i < count; i++) {
				AUPreset *preset = (AUPreset *) CFArrayGetValueAtIndex(presets, i);
				NSString *presetName = (NSString *) preset->presetName;
				//				[popup addItemWithTitle: presetName];
				//				[[popup itemWithTitle: presetName] setEnabled: YES];
				
				//				item = [[presetMenu addItemWithTitle: presetName 
				//											  action: @selector(changePreset:) 
				//									   keyEquivalent: @""] 
				//						autorelease];
				NSMenuItem *item = [[[self presetMenu] addItemWithTitle: presetName 
																 action: @selector(changePreset:) 
														  keyEquivalent: @""]
									autorelease];
				[item setTag:i];
				
				//				NSLog(@"currentPreset=%s %i", currentPreset.presetName, currentPreset.presetNumber);
				if (currentPreset.presetNumber == i) {
					//					id item = [popup itemWithTitle: presetName];
					//					[popup selectItem: item];
					//					NSLog(@"kPresetToolbarItemID: %@", item);
					[self setPresetPicked:i];
				}
			}
			[[self presetMenu] addItem:[NSMenuItem separatorItem]];
			//		} else {
			//			NSMenuItem *item = [[presetMenu addItemWithTitle: @"load/save" 
			//													  action: nil
			//											   keyEquivalent: @""]
			//								autorelease];
			//			[item setEnabled:NO];
		}
		[self scanPresets];
		for (NSDictionary *dict in [self presetsTree]) {
			NSInteger tag = 0;
			NSString *domain = [dict objectForKey:@"presetName"];
			NSMenuItem *item = [[[self presetMenu] addItemWithTitle: domain
															 action: nil 
													  keyEquivalent: @""]
								autorelease];
			[item setEnabled:NO];
			SEL selector;
			if ([domain isEqualToString:FactoryDomain]) {
				selector = @selector(changePreset:);
			} else if ([domain isEqualToString:UserDomain]) {
				selector = @selector(loadUserPreset:);
			} else if ([domain isEqualToString:LocalDomain]) {
				selector = @selector(loadLocalPreset:);
			} else {
			}
			//			for (NSString *key in [dict allKeys]) {				
			//				NSLog(@"key: %@ %@ [%@]", key, [[dict objectForKey:key] class], [dict objectForKey:key]);
			//			}
			NSArray *children = [dict objectForKey:@"children"];
			for (NSDictionary *child in children) {
				NSLog(@"child: %@ [%@]", child, [child class]);
				NSMenuItem *item = [[[self presetMenu] addItemWithTitle: [child objectForKey:@"presetName"] 
																 action: selector 
														  keyEquivalent: @""]
									autorelease];
				[item setTag:tag++];
			}
			[[self presetMenu] addItem:[NSMenuItem separatorItem]];
		}
		//		[[self presetsTree] valueForKey:FactoryDomain];
		//NSLog(@"presetsTree: %@", [self presetsTree]);
		for (UInt32 i = 0; i < [[self presetsTree] count]; i++) {
			NSDictionary *dict = [[self presetsTree] objectAtIndex:i];
			//NSLog(@"presetsTree at: %d = %@ %@", i, [object class], object);
			//NSDictionary
			//NSLog(@"%@", [dict allKeys]);
			for (NSString *key in [dict allKeys]) {
				id value = [dict valueForKey:key];
				NSLog(@"key:%@ %@ %@", key, [value class], value);
				if ([key isEqualToString:@"children"]) {
					NSLog(@"size=%d", [value count]);
				}
			}
		}
		//		for (NSDictionary dict in [self presetsTree]) {
		//			NSLog(@"");
		//		}
		if (FALSE) {
			NSArray *localPresetsArray = [self localPresets];
			for (NSUInteger i = 0; i < [localPresetsArray count]; i++) {
				//			AUPreset *preset = [presetArray objectAtIndex:i];
				//			NSString *presetName = (NSString *) preset->presetName;
				id object = [localPresetsArray objectAtIndex:i];
				NSLog(@"localPresets: class=%@ object=%@", [object class], object);
			}
			[[self presetMenu] addItem:[NSMenuItem separatorItem]];
			NSArray *userPresetstArray = [self userPresets];
			for (NSUInteger i = 0; i < [userPresetstArray count]; i++) {
				//			AUPreset *preset = [presetArray objectAtIndex:i];
				//			NSString *presetName = (NSString *) preset->presetName;
				id object = [userPresetstArray objectAtIndex:i];
				NSLog(@"userPresets: class=%@ object=%@", [object class], object);
			}
		}
		
		// load & save has no tags.
		//		[[[self presetMenu] addItemWithTitle: @"load Preset" 
		//							   action: @selector(loadPreset:)
		//						keyEquivalent: @""] 
		//		 autorelease];
		[[[self presetMenu] addItemWithTitle: @"save Preset"  
									  action: @selector(savePreset:)
							   keyEquivalent: @""] 
		 autorelease];
		[[self presetMenu] addItem:[NSMenuItem separatorItem]];
		[[[self presetMenu] addItemWithTitle: @"import Preset" 
									  action: @selector(loadPresetFromFile:)
							   keyEquivalent: @""] 
		 autorelease];
		[[[self presetMenu] addItemWithTitle: @"export Preset"  
									  action: @selector(savePresetToFile:)
							   keyEquivalent: @""] 
		 autorelease];
		//		[[presetMenu addItemWithTitle: @"load local Preset" 
		//							   action: @selector(loadPreset:)
		//						keyEquivalent: @""] 
		//		 autorelease];
		//		[[presetMenu addItemWithTitle: @"save local Preset"  
		//							   action: @selector(savePreset:)
		//						keyEquivalent: @""] 
		//		 autorelease];
		//		[presetMenu addItem:[NSMenuItem separatorItem]];
		//		[[presetMenu addItemWithTitle: @"load user Preset" 
		//							   action: @selector(loadPreset:)
		//						keyEquivalent: @""] 
		//		 autorelease];
		//		[[presetMenu addItemWithTitle: @"save user Preset"  
		//							   action: @selector(savePreset:)
		//						keyEquivalent: @""] 
		//		 autorelease];
		[popup setMenu:[self presetMenu]];
		[popup selectItemAtIndex:[self presetPicked]];
		//		[toolbarItem setLabel: @"Preset"];
		//		[toolbarItem setView: popup];
		//		[toolbarItem setTarget: self];
		[self setPresetPopUpView:[[NSView alloc] initWithFrame:[popup frame]]];
		[[self presetPopUpView] addSubview:popup];
		
		[menuItem setSubmenu:[self presetMenu]];
		//		[menuItem setSubmenu: [popup menu]];
		toolbarItem = [self toolbarItemWithIdentifier:kPresetToolbarItemID
                                                label:@"Preset"
                                          paleteLabel:@"Preset"
                                              toolTip:@"set selected Preset"
                                               target:self
                                          itemContent:[self presetPopUpView]
                                               action:nil
                                                 menu:[self presetMenu]];
	}
	return toolbarItem;
}

//--------------------------------------------------------------------------------------------------
// This method is required of NSToolbar delegates.
// It takes an identifier, and returns the matching NSToolbarItem. It also takes a parameter telling
// whether this toolbar item is going into an actual toolbar, or whether it's going to be displayed
// in a customization palette.
//--------------------------------------------------------------------------------------------------
- (NSToolbarItem *)toolbar2:(NSToolbar *)toolbar itemForItemIdentifier:(NSString *)itemIdentifier willBeInsertedIntoToolbar:(BOOL)flag
{
	NSLog(@"toolbar2:itemForItemIdentifier:willBeInsertedIntoToolbar: has been called");
	NSToolbarItem *toolbarItem = [[[NSToolbarItem alloc] initWithItemIdentifier:itemIdentifier] autorelease];;
	
	// We create and autorelease a new NSToolbarItem, and then go through the process of setting up its
	// attributes from the master toolbar item matching that identifier in our dictionary of items.
	if ([itemIdentifier isEqualToString:kViewStyleToolbarItemID]) {
		// 1) Font style toolbar item
		//		toolbarItem = [self toolbarItemWithIdentifier:kViewStyleToolbarItemID
		//                                                               label:@"View Style"
		//                                                         paleteLabel:@"View Style"
		//                                                             toolTip:@"Change your AudioUnit view style"
		//                                                              target:self
		//                                                         itemContent:viewStylePopUpView
		//                                                              action:nil
		//                                                                menu:viewStyleMenu];
		NSPopUpButton *popup = [[NSPopUpButton alloc] initWithFrame: NSMakeRect(0, 0, 150, 25)];
		//[[popup cell] setBordered:YES];
 		[[popup cell] setBezeled:YES];
 		[[popup cell] setControlSize:NSSmallControlSize];
 		
		// static CFStringEncodings    _cfEncodings[] = {
		//     kCFStringEncodingShiftJIS,              // Japanese (Shift JIS)
		//     kCFStringEncodingISO_2022_JP,           // Japanese (ISO 2022-JP)
		//     kCFStringEncodingEUC_JP,                // Japanese (EUC)
		//     kCFStringEncodingShiftJIS_X0213_00,     // Japanese (Shift JIS X0213)
		//     kCFStringEncodingInvalidId, 
		//     kCFStringEncodingUTF8,                  // Unicode (UTF-8)
		//     kCFStringEncodingInvalidId, 
		//     kCFStringEncodingISOLatin1,             // Western (ISO Latin 1)
		//     kCFStringEncodingMacRoman,              // Western (Mac OS Roman)
		// };
		//  		NSMenu *menu = [[[NSMenu alloc] initWithTitle:@"View Style"] autorelease];
		//  		for (int i = 0; i < sizeof(_cfEncodings) / sizeof(CFStringEncodings); i++) {
		//  			//id <NSMenuItem> menuItem;
		//  			NSMenuItem *menuItem;
		//  			
		//  			if (_cfEncodings[i] == kCFStringEncodingInvalidId) {
		//  				menuItem = [NSMenuItem separatorItem];
		//  			} else {
		//  				 NSStringEncoding encoding = CFStringConvertEncodingToNSStringEncoding(_cfEncodings[i]);;
		//  				 NSString *encodingName = [NSString localizedNameOfStringEncoding:encoding];
		//  				 menuItem = [[[NSMenuItem alloc] 
		// 											initWithTitle:encodingName 
		// 											action:@selector(_popUpItemAction:) 
		// 											keyEquivalent:@""] autorelease];
		// 				[menuItem setTag:encoding];
		//  			}
		//  			[menu addItem:menuItem];
		//  		}
		// 		[popup setMenu:menu];
		//  		NSString *object1 = @"PHP";
		// 		NSString *object2 = @"SQL";
		// 		NSString *object3 = @"HTML";
		// 		NSString *object4 = @"Objective C";
		// 		NSMutableArray *myArray;
		// 		myArray= [NSMutableArray arrayWithObjects: object1, object2, object3, object4, nil];
		// 		[popup removeAllItems];
		// 		[popup addItemsWithTitles: myArray];
		// 		[toolbarItem setLabel: @"View Style"];
		// 		[toolbarItem setView: popup];
		// 		[menuItem setSubmenu: submenu];
		// 		[toolbarItem setMenuFormRepresentation: menuItem];
		
		//  		NSMenu *subMenu = [[NSMenu alloc] initWithTitle: @""];
		// 		[subMenu setDelegate:self];
		// 		NSMenuItem *menuItem01 = [[NSMenuItem alloc]
		// 												initWithTitle:@"item01"
		// 												action:@selector(changeViewStyle:)
		// 												keyEquivalent:@"1"];
		// 		[subMenu insertItem:menuItem01 atIndex:0];
		// 		NSMenuItem *menuItem02 = [[NSMenuItem alloc]
		// 												initWithTitle:@"item02"
		// 												action:@selector(changeViewStyle:)
		// 												keyEquivalent:@"2"];
		// 		[subMenu insertItem:menuItem02 atIndex:1];
		// 		[submenu autorelease];
		// 		[menuItem01 autorelease];
		// 		[menuItem02 autorelease];
		// 		[popup setSubmenu: submenu];
		// 		//[toolbarItem setMenuFormRepresentation: menuItem];
 		
		//
		if (true) {
			id item;
			NSMenuItem *menuItem;
			NSMenu *submenu;
			
			[popup setAutoenablesItems: NO];
			[popup setAutoenablesItems: NO];
			[popup setAction: @selector(changeViewStyle:)];
			
			[popup addItemWithTitle: @"Marguerite"];
			[[popup itemWithTitle: @"Marguerite"] setEnabled: YES];
			[popup addItemWithTitle: @"Julie"];
			[[popup itemWithTitle: @"Julie"] setEnabled: YES];
			[popup addItemWithTitle: @"Liv"];
			[[popup itemWithTitle: @"Liv"] setEnabled: YES];
			[popup addItemWithTitle: @"Juliette"];
			[[popup itemWithTitle: @"Juliette"] setEnabled: YES];
			
			item = [popup itemWithTitle: @"Julies"];
			[popup selectItem: item];
			
			[toolbarItem setLabel: @"View Style"];
//			[toolbarItem setView: popup];
			
			menuItem = [[NSMenuItem alloc] initWithTitle: @"More..." 
												  action: NULL
										   keyEquivalent: @""];
			submenu = [[NSMenu alloc] initWithTitle: @""];
			[submenu addItemWithTitle: @"Marguerite" 
							   action: @selector(changeViewStyle:) 
						keyEquivalent: @"1"];
			[submenu addItemWithTitle: @"Julie" 
							   action: @selector(changeViewStyle:) 
						keyEquivalent: @"2"];
			[submenu addItemWithTitle: @"Liv" 
							   action: @selector(changeViewStyle:) 
						keyEquivalent: @"3"];
			[submenu addItemWithTitle: @"Juliette" 
							   action: @selector(changeViewStyle:) 
						keyEquivalent: @"5"];
			[submenu autorelease];
			[menuItem autorelease];
			[menuItem setSubmenu: submenu];
			[toolbarItem setMenuFormRepresentation: menuItem];
		}
		//       [menuItem setSubmenu: AUTORELEASE(submenu)];
		//       [toolbarItem setMenuFormRepresentation: AUTORELEASE(menuItem)];
		
		//
 		
		// 	} else if ([itemIdentifier isEqualToString:kFontSizeToolbarItemID]) {   
		// 		// 2) Font size toolbar item
		// 		toolbarItem = [self toolbarItemWithIdentifier:kFontSizeToolbarItemID
		//                                                 label:@"Font Size"
		//                                           paleteLabel:@"Font Size"
		//                                               toolTip:@"Grow or shrink the size of your font"
		//                                                target:self
		//                                           itemContent:fontSizeView
		//                                                action:nil
		//                                                  menu:fontSizeMenu];
		// 	} else if ([itemIdentifier isEqualToString:kBlueLetterToolbarItemID]) {    
		// 		// 3) Blue text toolbar item
		// 		// often using an image will be your standard case.  You'll notice that a selector is passed
		// 		// for the action (blueText:), which will be called when the image-containing toolbar item is clicked.
		// 		//
		// 		toolbarItem = [self toolbarItemWithIdentifier:kBlueLetterToolbarItemID
		//                                                 label:@"Blue Text"
		//                                           paleteLabel:@"Blue Text"
		//                                               toolTip:@"This toggles blue text on/off"
		//                                                target:self
		//                                           itemContent:[NSImage imageNamed:@"blueLetter.tif"]
		//                                                action:@selector(blueText:)
		//                                                  menu:nil];        
	}
	
	return toolbarItem;
}

//- (void) reflectMenuSelection:(id)sender
//{
//	NSInteger itemIndex;
//	
//	if ([NSStringFromClass([sender class]) isEqual:@"NSMenuItem"]) {
//		// for ordinary NSMenus, the title is item #0, so we have to offset things
//		itemIndex = [[sender menu] indexOfItem:sender];
//	} else {
//		// this is an NSPopUpButton, so the first useful item really is #0
//		itemIndex = [sender indexOfSelectedItem];
//	}
//	NSLog(@"reflectMenuSelection: %@ %s", sender, [sender titleOfSelectedItem]);
//	viewStylePicked = itemIndex + 1;
//}

/* 
 Returns the ordered list of items to be shown in the toolbar by default.   
 If during initialization, no overriding values are found in the user defaults, 
 or if the user chooses to revert to the default items this set will be used.
 */

//--------------------------------------------------------------------------------------------------
// This method is required of NSToolbar delegates.  It returns an array holding identifiers for the default
// set of toolbar items.  It can also be called by the customization palette to display the default toolbar.  
//--------------------------------------------------------------------------------------------------
- (NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar *)toolbar
{
	NSLog(@"toolbarDefaultItemIdentifiers: has been called");
	return [NSArray arrayWithObjects:	kViewStyleToolbarItemID,
										kPresetToolbarItemID,
			// 										kBlueLetterToolbarItemID,
			// 										NSToolbarPrintItemIdentifier,
										nil];
	// note:
	// that since our toolbar is defined from Interface Builder, an additional separator and customize
	// toolbar items will be automatically added to the "default" list of items.
}

/* 
 Returns the list of all allowed items by identifier.  
 By default, the toolbar does not assume any items are allowed, even the separator.  
 So, every allowed item must be explicitly listed.  
 The set of allowed items is used to construct the customization palette.  
 The order of items does not necessarily guarantee the order of appearance in the palette.  
 At minimum, you should return the default item list.
 */

//--------------------------------------------------------------------------------------------------
// This method is required of NSToolbar delegates.  It returns an array holding identifiers for all allowed
// toolbar items in this toolbar.  Any not listed here will not be available in the customization palette.
//--------------------------------------------------------------------------------------------------
- (NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar *)toolbar
{
	NSLog(@"toolbarAllowedItemIdentifiers: has been called"); 
	return [NSArray arrayWithObjects:	kViewStyleToolbarItemID,
			// 										kFontSizeToolbarItemID,
			// 										kBlueLetterToolbarItemID,
										kPresetToolbarItemID,
										NSToolbarSpaceItemIdentifier,
										NSToolbarFlexibleSpaceItemIdentifier,
			// 										NSToolbarPrintItemIdentifier,
			nil];
	// note:
	// that since our toolbar is defined from Interface Builder, an additional separator and customize
	// toolbar items will be automatically added to the "default" list of items.
}

#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_3
/* 
 Optional method. 
 Those wishing to indicate item selection in a toolbar should implement this method 
 to return a non-empty array of selectable item identifiers.  If implemented, 
 the toolbar will remember and display the selected item with a special highlight.  
 A selected item is one whose item identifier matches the current selected item identifier.  
 Clicking on an item whose identifier is selectable will automatically update the toolbars 
 selected item identifier when possible. (see setSelectedItemIdentifier: for more details)
 */

- (NSArray *)toolbarSelectableItemIdentifiers:(NSToolbar *)toolbar
{
	NSLog(@"toolbarSelectableItemIdentifiers: has been called");
	// do nothing
}

#endif

#pragma mark NSToolbarDelegate Notifications

/*  Notifications */

/* 
 Before an new item is added to the toolbar, this notification is posted.  
 This is the best place to notice a new item is going into the toolbar.  
 For instance, if you need to cache a reference to the toolbar item or need to set up some initial state, 
 this is the best place to do it.   
 The notification object is the toolbar to which the item is being added.  
 The item being added is found by referencing the @"item" key in the userInfo.
 */

//--------------------------------------------------------------------------------------------------
// This is an optional delegate method, called when a new item is about to be added to the toolbar.
// This is a good spot to set up initial state information for toolbar items, particularly ones
// that you don't directly control yourself (like with NSToolbarPrintItemIdentifier here).
// The notification's object is the toolbar, and the @"item" key in the userInfo is the toolbar item
// being added.
//--------------------------------------------------------------------------------------------------
- (void)toolbarWillAddItem:(NSNotification *)notif
{
	NSLog(@"toolbarWillAddItem: has been called");
	NSToolbarItem *addedItem = [[notif userInfo] objectForKey:@"item"];
	
	// Is this the printing toolbar item?  If so, then we want to redirect it's action to ourselves
	// so we can handle the printing properly; hence, we give it a new target.
	//
	if ([[addedItem itemIdentifier] isEqual: NSToolbarPrintItemIdentifier]) {
		[addedItem setToolTip:@"Print your document"];
		[addedItem setTarget:self];
	}
}

/* 
 After an item is removed from a toolbar the notification is sent.  
 This allows the chance to tear down information related to the item that may have been cached.  
 The notification object is the toolbar from which the item is being removed.  
 The item being removed is found by referencing the @"item" key in the userInfo.
 */
- (void)toolbarDidRemoveItem: (NSNotification *)notification
{
	NSLog(@"toolbarDidRemoveItem: has been called");
	// do nothing
}

@end

#endif //TARGET_OS_IPHONE
