//
//  PluginUIController+Interaction.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/09.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import "PluginUIController+Interaction.h"


@implementation PluginUIController (Interaction)

- (NSAlert *)inform:(NSString *)messageText informativeText:(NSString *)informativeText
{
	return [self confirmOrOther:messageText
				informativeText:informativeText
					  useCancel:NO
					   useOther:NO
				otherButtonName:nil];	
}

- (NSAlert *)confirm:(NSString *)messageText informativeText:(NSString *)informativeText
{
	return [self confirmOrOther:messageText
				informativeText:informativeText
					  useCancel:YES
					   useOther:NO
				otherButtonName:nil];
}

- (NSAlert *)confirmOrOther:(NSString *)messageText 
			informativeText:(NSString *)informativeText 
				  useCancel:(BOOL)useCancel 
				   useOther:(BOOL)useOther 
			otherButtonName:(NSString *)otherButtonName
{
	NSString *alternateButton = nil;
	NSString *otherButton = nil;
	
	if (useCancel) alternateButton = @"cancel";
	if (useOther) otherButton = otherButtonName;
//	NSAlert *alert = [NSAlert alertWithMessageText: messageText
//									 defaultButton: @"OK" 
//								   alternateButton: alternateButton 
//									   otherButton: otherButton
//						 informativeTextWithFormat: informativeText ];
	NSAlert *alert = [[NSAlert alloc] init];
	[alert addButtonWithTitle:@"OK"];
	[alert addButtonWithTitle:alternateButton];
	[alert setMessageText:messageText];
	[alert setInformativeText:informativeText];
//	[alert setAlertStyle:NSWarningAlertStyle];
	[alert setAlertStyle:NSInformationalAlertStyle];
//	[alert setAlertStyle:NSCriticalAlertStyle];
//	[alert beginSheetModalForWindow:[self window] 
//					  modalDelegate:self 
//					 didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:)
//						contextInfo:&response];
//	return [alert runModalSheet];
	NSLog(@"here!");
	return alert;
}

//- (void) alertDidEnd:(NSAlert *) alert returnCode:(int) returnCode contextInfo:(int *) contextInfo
//{
//    *contextInfo = returnCode;
//}

@end
