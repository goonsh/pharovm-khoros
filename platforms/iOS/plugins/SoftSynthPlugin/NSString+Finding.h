//
//  NSString+Finding.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/08.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSString (Finding)

- (BOOL) containsString:(NSString *)substring;
- (NSString *)match:(NSString *)string;

@end
