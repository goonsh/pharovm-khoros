//
//  NSString+Finding.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/08.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <ExceptionHandling/NSExceptionHandler.h>

#import "NSString+Finding.h"
#import <regex.h>

@implementation NSString (Finding)

- (BOOL) containsString:(NSString *)substring
{    
    NSRange range = [self rangeOfString:substring];
    BOOL found = (range.location != NSNotFound);
    return found;
}

//- (void)printStackTrace:(NSException *)e
//{
//    NSString *stack = [[e userInfo] objectForKey:NSStackTraceKey];
//    if (stack) {
//        NSTask *ls = [[NSTask alloc] init];
//        NSString *pid = [[NSNumber numberWithInt:[[NSProcessInfo processInfo] processIdentifier]] stringValue];
//        NSMutableArray *args = [NSMutableArray arrayWithCapacity:20];
//		
//        [args addObject:@"-p"];
//        [args addObject:pid];
//        [args addObjectsFromArray:[stack componentsSeparatedByString:@"  "]];
//        // Note: function addresses are separated by double spaces, not a single space.
//		
//        [ls setLaunchPath:@"/usr/bin/atos"];
//        [ls setArguments:args];
//        [ls launch];
//        [ls release];
//		
//    } else {
//        NSLog(@"No stack trace available.");
//    }
//}
//
//- (BOOL)exceptionHandler:(NSExceptionHandler *)sender shouldLogException:(NSException *)exception mask:(unsigned int)mask
//{
//    [self printStackTrace:exception];
//    return YES;
//}

//How to use regular expression in Cocoa/Objective-C
//http://jongampark.wordpress.com/2010/05/15/how-to-use-regular-expression-in-cocoaobjective-c/
- (NSString *)match:(NSString *)regexPattern
{
	int isFail;
	regex_t regex;
	// track up to 5 maches. Actually only one is needed.
	regmatch_t pmatch[5];
	const char *sourceCString;
	char errorMessage[512], foundCString[16];
	NSString *errorMessageString;
	NSString *foundString = nil;
	sourceCString = [self UTF8String];
	
	// setup the regular expression
	@try {
		NSException *exception;
		const char *pattern;
		pattern = [regexPattern cStringUsingEncoding:NSUTF8StringEncoding];
		if ((isFail = regcomp(&regex, pattern, REG_EXTENDED)) == true) {
			regerror(isFail, &regex, errorMessage, 512);
			errorMessageString = [NSString stringWithCString:errorMessage encoding:NSUTF8StringEncoding];
			exception = [NSException exceptionWithName:@"RegexException"
												reason:errorMessageString 
											  userInfo:nil];
            @throw exception;
		} else {
			if ((isFail = regexec(&regex, sourceCString, 5, pmatch, 0)) == true) {
				regerror(isFail, &regex, errorMessage, 512 );
				errorMessageString = [NSString stringWithCString:errorMessage encoding:NSUTF8StringEncoding];
				exception = [NSException exceptionWithName:@"RegexException"
													reason:errorMessageString
												  userInfo:nil];
				@throw exception;
			} else {
				snprintf(foundCString, (size_t) (pmatch[0].rm_eo - pmatch[0].rm_so + 1), 
						 "%s", &sourceCString[pmatch[0].rm_so] );
				foundString = [NSString stringWithCString:foundCString encoding:NSUTF8StringEncoding];
			}
		}
	}
	@catch (NSException *ex) {
		NSLog(@"%@ occurred due to %@", [ex name], [ex reason]);
		NSArray *addresses = [ex callStackReturnAddresses];
		NSArray *symbols = [ex callStackSymbols];
		NSLog(@"call return addresses: %@", addresses);
		NSLog(@"current call symbols: %@", symbols);
		NSLog(@"application-specific data : %@", [ex userInfo]);
		
		
//		NSMutableDictionary *errorDict = [NSMutableDictionary dictionary];
//		[errorDict setObject:[NSString stringWithFormat:@"Error %@: %@ %@", self, regexPattern, [ex reason]] forKey:OSAScriptErrorMessage];
//		[errorDict setObject:[NSNumber numberWithInt:errOSAGeneralError] forKey:OSAScriptErrorNumber];
////		*errorInfo = errorDict;
//		NSLog(@"application-specific data : %@", errorDict);
//		[self printStackTrace:ex];
	}
	@finally {
		regfree(&regex);
	}
	return foundString;
}

@end
