//
//  PluginUIToolbarController+Presets.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/16.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//
#import <AudioToolbox/AudioUnitUtilities.h>	//kAUParameterListener_AnyParameter, AUParameterListenerNotify

#import "PluginUIToolbarController+Presets.h"


@implementation PluginUIToolbarController (Presets)


//--------------------------------------------------------------------------------------------------
//    getCurrentPreset:
//--------------------------------------------------------------------------------------------------
- (AUPreset)getCurrentPreset:(AudioUnit)unit
{
	return [[self class] getCurrentPreset:unit];
}

//**************************************************************************************************
//    getCurrentPreset:
//
//		get current preset for given autio unit
//		@param AudioUnit audioUnit
//		@return AUPreset
//**************************************************************************************************
+ (AUPreset)getCurrentPreset:(AudioUnit)unit
{
	NSLog(@"getCurrentPreset: has been called.");
	
	AUPreset currentPreset;
	UInt32 size = sizeof(currentPreset);
    OSStatus result = AudioUnitGetProperty(unit,
										   kAudioUnitProperty_PresentPreset,
										   kAudioUnitScope_Global,
										   0,
										   &currentPreset,
										   &size);
	if (result == kAudioUnitErr_InvalidProperty) {
		size = sizeof(currentPreset);
		result = AudioUnitGetProperty(unit,
									  kAudioUnitProperty_CurrentPreset,
									  kAudioUnitScope_Global,
									  0,
									  &currentPreset,
									  &size);
		if (result == noErr) {
			if (currentPreset.presetName) {
				CFRetain((currentPreset.presetName));
			}
		}
	}
	return currentPreset;
}

//**************************************************************************************************
//    getCurrentPresetName:
//
//		get current preset name for given autio unit
//		@param AudioUnit audioUnit
//		@return (NSString *) name of AUPreset
//**************************************************************************************************
+ (NSString *)getCurrentPresetName:(AudioUnit)unit
{
	AUPreset preset = [self getCurrentPreset:unit];
	return (NSString *) preset.presetName;
}

//--------------------------------------------------------------------------------------------------
//    getCurrentPresetName:
//--------------------------------------------------------------------------------------------------
- (NSString *)getCurrentPresetName:(AudioUnit)unit
{
	return [[self class] getCurrentPresetName:unit];
}

//**************************************************************************************************
//    notifyAUParameterListenerOfParameterChanges:
//
//		Notify listeners of a past parameter change.
//		@param AudioUnit audioUnit
//		@return none
//**************************************************************************************************
+ (void)notifyAUParameterListenerOfParameterChanges: (AudioUnit)unit
{
	AudioUnitParameter changedUnit;
	changedUnit.mAudioUnit = unit;
	changedUnit.mParameterID = kAUParameterListener_AnyParameter;
	OSStatus result = AUParameterListenerNotify(NULL, NULL, &changedUnit);
	if (result != noErr) {
		NSLog(@"AUParameterListenerNotify failed: %i", result);
	}
}

//--------------------------------------------------------------------------------------------------
//    audioUnotifyAUParameterListenerOfParameterChanges:
//		
//		Notify listeners of a past parameter change.
//		@param AudioUnit audioUnit
//		@return none
//--------------------------------------------------------------------------------------------------
- (void)notifyAUParameterListenerOfParameterChanges: (AudioUnit)unit
{
	[[self class] notifyAUParameterListenerOfParameterChanges:unit];
}

//**************************************************************************************************
//    getPresets:
//
//		get preset for given autio unit
//		@param AudioUnit audioUnit
//		@return CFArrayRef array of presets
//**************************************************************************************************
+ (CFArrayRef)getPresets:(AudioUnit)unit
{
	CFArrayRef presets = NULL;
	UInt32 propertySize = sizeof(presets);
	AudioUnitGetProperty(unit,
						 kAudioUnitProperty_FactoryPresets,
						 kAudioUnitScope_Global,
						 0,
						 &presets,
						 &propertySize);
#ifdef __DEBUG__
	{
		int count, i;
		
		if (presets != NULL) {
			count = CFArrayGetCount(presets);
			for (i = 0; i < count; i++) {
				AUPreset *preset = (AUPreset *) CFArrayGetValueAtIndex(presets, i);
				printf("%ld: %s\n", preset->presetNumber, CFStringGetCStringPtr(preset->presetName, encoding));
			}
		}
	}
#endif //__DEBUG__
	return presets;
}

//**************************************************************************************************
//    setPreset:to:
//
//		set preset for given autio unit
//		@param AudioUnit audioUnit
//		@param UInt32 presetNumber answerd by getPreset(audioUnit)
//		@return OSStatus 
//**************************************************************************************************
+ (OSStatus)setPreset:(AudioUnit)unit to:(UInt32)presetNumber
{
	AUPreset newPreset;
	OSStatus result;
	
	newPreset.presetNumber = presetNumber;
	result = AudioUnitSetProperty(unit,
								  kAudioUnitProperty_PresentPreset,
								  kAudioUnitScope_Global,
								  0,
								  &newPreset,
								  sizeof(AUPreset));
	[self notifyAUParameterListenerOfParameterChanges:unit];
	NSLog(@"err=%d", result);
	return result;
}

//**************************************************************************************************
//    setPreset:named:
//
//		set preset for given autio unit
//		@param AudioUnit audioUnit
//		@param CFStringRef presetName answerd by getPreset(audioUnit)
//		@return OSStatus 
//**************************************************************************************************
+ (OSStatus)setPreset:(AudioUnit)unit named:(CFStringRef)presetName
{
	AUPreset newPreset;
	OSStatus result;
	
	newPreset.presetName = presetName;
	result = AudioUnitSetProperty(unit,
								  kAudioUnitProperty_PresentPreset,
								  kAudioUnitScope_Global,
								  0,
								  &newPreset,
								  sizeof(AUPreset));
	[self notifyAUParameterListenerOfParameterChanges:unit];
	NSLog(@"err=%d", result);
	return result;
}

//**************************************************************************************************
//    setPreset:presetName:presetNumber:
//
//		set preset for given autio unit
//		@param AudioUnit audioUnit
//		@param NSString *presetNumber answerd by getPreset(audioUnit)
//		@param UInt32 presetNumber answerd by getPreset(audioUnit)
//		@return OSStatus 
//**************************************************************************************************
+ (OSStatus)setPreset:(AudioUnit)unit presetName:(CFStringRef)presetName presetNumber:(UInt32) presetNumber
{
	AUPreset newPreset;
	OSStatus result;
	
	newPreset.presetNumber = presetNumber;
	newPreset.presetName = presetName;
	result = AudioUnitSetProperty(unit,
								  kAudioUnitProperty_PresentPreset,
								  kAudioUnitScope_Global,
								  0,
								  &newPreset,
								  sizeof(AUPreset));
	[self notifyAUParameterListenerOfParameterChanges:unit];
	NSLog(@"err=%d", result);
	return result;
}


@end
