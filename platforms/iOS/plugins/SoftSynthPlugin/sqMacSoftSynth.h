/*
	sqMacSoftSynth.h
	SoftSynthPlugin

	Created by NISHIHARA Satoshi on 13/08/18.
	Copyright 2013 goonsh@gmail.com. All rights reserved.
 */

#ifndef __encoding__
#define __encoding__
static const CFStringEncoding encoding = kCFStringEncodingUTF8;	//kCFStringEncodingMacRoman;
#endif //__encoding__

CFStringRef getAudioUnitName(AudioUnit unit);
int inform(const char *messageText, const char *informativeText);
int confirm(const char *messageText, const char *informativeText);
int confirmOrOther(const char *messageText, const char *informativeText, Boolean useCancel, Boolean useOther, const char *otherButtonName);
