//
//  PluginUIController.m
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/07/29.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//
//	2013/08/14 fixed: get CocoaViewBundlePath through CFURLCopyPath.
//	select "AUDelay" will cause crash.
//	select "MiniSpillage" will cause crash.
//	select "AUMultibandCompressor" will cause crash.
//	comment out CFRelease(cocoaViewInfo->mCocoaAUViewBundleLocation); at getCocoaView()
//	it can show edit view for "AUDelay" and "AUMultibandCompressor"
//

#import <objc/objc-runtime.h>

#import <CoreAudioKit/CoreAudioKit.h>
#import <AudioUnit/AUCocoaUIView.h>
#import <CoreAudioKit/AUGenericView.h>
#import <AudioToolbox/AudioUnitUtilities.h>

#include <Carbon/Carbon.h>

#import "PluginUIController.h"
#import "PluginUIController+Interaction.h"
#import "PluginUIController+AudioUnit.h"
#import "NSArray+UnaryAccessors.h"
#import "NSString+Finding.h"

#import <AssertMacros.h>
#import "sqMacSoftSynth.h"
#import "debug.h"
#import "interaction.h"
#import "notification.h"
#import "AddSubclassesToArray.h"
#import "Log.h"

#if !(TARGET_OS_IPHONE)

static pascal OSStatus CarbonWindowEventHandler(EventHandlerCallRef nextHandlerRef, EventRef event, void *userData);

//NSString *debugFunctionMsg(const char *func, const char *file, int line, NSString *fmt);
//void debugLog(NSString *format, ...);
//
//NSString *debugFunctionMsg(const char *func, const char *file, int line, NSString *fmt) {
//	NSString *message;
//	
//	message = [NSString stringWithFormat: @"File %s: %d. In %s %@",
//			   file, line, func, fmt];
//	return message;
//}
//
////void debugLog(NSString *format, ...) {
//void debugLog(NSString *format, ...) {
//	va_list ap;
//	va_start(ap, format);
//	NSString *s = debugFunctionMsg(__PRETTY_FUNCTION__, __FILE__, __LINE__, [NSString stringWithFormat: format, ap]);
//	NSLog(@"%@", s);
//	va_end(ap);
//}
//
//#define debugSet(x)	YES
//#define NSDebugFLLog(level, format, args...) \
//do { if (debugSet(level) == YES) { \
//NSString *s = debugFunctionMsg( \
//__PRETTY_FUNCTION__, __FILE__, __LINE__, \
//[NSString stringWithFormat: format, ##args]); \
//NSLog(@"%@", s); }} while (0)
//
//void NSDebugFLLog(level, NSString *format, args...) {
//	NSString *s = debugFunctionMsg(__PRETTY_FUNCTION__, __FILE__, __LINE__,
//								   [NSString stringWithFormat: format, ##args]);
//	NSLog(@"%@", s);
//}

// private methods: class extentions
#pragma mark -
//@interface PluginUIController (Private)
@interface PluginUIController ()
+ (CFStringRef)getAudioComponentName:(AudioComponent)component;
+ (NSString *)getAudioUnitName:(AudioUnit)unit;
- (void)initWithCocoaViewForUnit:(AudioUnit)unit;
- (void)initWithCarbonViewForUnit:(AudioUnit)unit;
- (void)initWithCocoaGenericViewForUnit:(AudioUnit)unit;
- (void)initWithCarbonGenericViewForUnit:(AudioUnit)unit;
- (void)initWithAudioUnitCocoaView:(NSView *)audioUnitView;
- (BOOL)audioUnitHasCocoaView:(AudioUnit)unit;
- (BOOL)audioUnitHasCarbonView:(AudioUnit)unit;

- (id)initWithAudioUnit:(AudioUnit)unit;

- (void)audioUnitChangedViewSize:(NSNotification *)notification;
+ (Class)detectClass:(Class)sourceClass useToolbar:(BOOL)useToolbar;

@end

#pragma mark -
//**************************************************************************************************
@implementation PluginUIController
//**************************************************************************************************
// private instance variables
//{
//	int te;
//}

@synthesize audioUnit = _audioUnit;
@synthesize auView = _AUView;
@synthesize carbonWindow = _carbonWindow;
@synthesize carbonView = _carbonView;
@synthesize audioUnitViewSize = _audioUnitViewSize;
@synthesize audioUnitViewType = _audioUnitViewType;

static NSMutableArray *PluginUIControllers = nil;

enum audioUnitViewType {
	hasCocoaView = 1,
	hasCarbonView = 2,
	hasCocoaGenericView = 4,
	hasCarbonGenericView = 8
};

//**************************************************************************************************
//    pluginClassIsValid:
//**************************************************************************************************
+ (BOOL)pluginClassIsValid:(Class)pluginClass 
{
	if ([pluginClass conformsToProtocol: @protocol(AUCocoaUIBase)]) {
		if ([pluginClass instancesRespondToSelector: @selector(interfaceVersion)] &&
		    [pluginClass instancesRespondToSelector: @selector(uiViewForAudioUnit:withSize:)]) {
			return YES;
		}
	}
    return NO;
}

//--------------------------------------------------------------------------------------------------
//    audioUnit
//--------------------------------------------------------------------------------------------------
- (AudioUnit)audioUnit
{
	NSLog(@"audioUnit: has been called.");
	return _audioUnit;
}

//--------------------------------------------------------------------------------------------------
//	dealloc
//--------------------------------------------------------------------------------------------------
- (void)dealloc
{
 	[[NSNotificationCenter defaultCenter] removeObserver:self 
													name:NSViewFrameDidChangeNotification 
												  object:_AUView];
	NSLog(@"here3");
	if (_carbonView) {
		CloseComponent(_carbonView);
	}
	NSLog(@"here4");
	if (_carbonWindow) {
		DisposeWindow(_carbonWindow);
	}
	NSLog(@"here5");
	if (_carbonEventHandler) {
		RemoveEventHandler(_carbonEventHandler);
	}
	NSLog(@"here6");
	//	_AUView = nil;
	[super dealloc];
}

- (BOOL)hasCocoaView
{
	return _audioUnitViewType & hasCocoaView;
}

- (BOOL)hasCarbonView
{
	return _audioUnitViewType & hasCarbonView;
}

- (BOOL)hasCocoaGenericView
{
	return _audioUnitViewType & hasCocoaGenericView;
}

- (BOOL)hasCarbonGenericView
{
	return _audioUnitViewType & hasCarbonGenericView;
}

- (id)basicInitialize
{
	NSLog(@"basicInitialize: _audioUnitViewType=%d", _audioUnitViewType);
	_audioUnitViewType = 0;
	return self;
}

#pragma mark designated initializer
//--------------------------------------------------------------------------------------------------
//	initWithAudioUnit:
//
//	This method is the designated initializer for PluginUIController.
//
//--------------------------------------------------------------------------------------------------
- (id)initWithAudioUnit:(AudioUnit)unit
{
	NSString *matched1 = [@"This is neat." match:@"(\\w+)\\s+(\\w+)\\s+(\\w+)"];
	NSString *matched12 = [@"load user preset" match:@".*user"];
	
	if ((self = [super initWithWindow:nil]) == nil) {
		return nil;
	}
	
	[self basicInitialize];
	[self setAudioUnit:unit];
	if ([self audioUnitHasCocoaView:unit]) {
		[self initWithCocoaViewForUnit:unit];
	} else if ([self audioUnitHasCarbonView:unit]) {
		[self initWithCarbonViewForUnit:unit];
	} else {
//		[self initWithCarbonGenericViewForUnit:unit];
		[self initWithCocoaGenericViewForUnit:unit];
	}
	[self setAudioUnitViewSize: [[self auView] frame].size];
	if ([[self window] delegate] == nil) {
		[[self window] setDelegate: self];
	}
//	NSDebugLog(@"initWithCocoaGenericViewForUnit: framesize %.2f@%.2f", AUView.frame.size.width, AUView.frame.size.height);
//	NSDebugFLLog(1, @"initWithCocoaGenericViewForUnit: framesize %.2f@%.2f", AUView.frame.size.width, AUView.frame.size.height);
	return self;
}

//#pragma mark PluginUIBaseController
//#pragma mark initializers
//--------------------------------------------------------------------------------------------------
//	initWithCocoaViewForUnit:
//--------------------------------------------------------------------------------------------------
- (void)initWithCocoaViewForUnit:(AudioUnit)unit
{
	UInt32  dataSize;
	Boolean isWriteable;
	AudioUnitCocoaViewInfo *cocoaViewInfo = NULL;
	UInt32  numberOfClasses;
	//	NSString *cocoaViewBundlePath = NULL;
	//	CFStringRef factoryClassName = NULL;
	NSURL 	 * cocoaViewBundlePath = nil;
	NSString * factoryClassName = nil;
	
	NSView *AUView = nil;
	
	// get AU's Cocoa view property
	OSStatus result = AudioUnitGetPropertyInfo(unit,
											   kAudioUnitProperty_CocoaUI,
											   kAudioUnitScope_Global,
											   0,
											   &dataSize,
											   &isWriteable);
	
	numberOfClasses = (dataSize - sizeof(CFURLRef)) / sizeof(CFStringRef);
	
	// getting the location / name of the necessary view factory bits
	if ((result == noErr) && (numberOfClasses > 0)) {
		cocoaViewInfo = (AudioUnitCocoaViewInfo *)malloc(dataSize);
		if(AudioUnitGetProperty(unit,
								kAudioUnitProperty_CocoaUI,
								kAudioUnitScope_Global,
								0,
								cocoaViewInfo,
								&dataSize) == noErr) {
			// we only take the first view in this plugin.
			// http://forum.cockos.com/showthread.php?t=39117
			//cocoaViewBundlePath = CFURLCopyPath(cocoaViewInfo->mCocoaAUViewBundleLocation);
			//cocoaViewBundlePath = (cocoaViewInfo->mCocoaAUViewBundleLocation);
			//			cocoaViewBundlePath = (NSString *) (CFURLCopyPath(cocoaViewInfo->mCocoaAUViewBundleLocation));
			cocoaViewBundlePath	= (NSURL *) cocoaViewInfo->mCocoaAUViewBundleLocation;
			factoryClassName = (NSString *) cocoaViewInfo->mCocoaAUViewClass[0];
		} else if (cocoaViewInfo != NULL) {
			free(cocoaViewInfo);
			cocoaViewInfo = NULL;
		}
	}
	
	// if we have everything we need, create the custom Cocoa view
	if (cocoaViewBundlePath && factoryClassName) {
		//NSBundle *viewBundle = [NSBundle bundleWithURL:(NSURL *)cocoaViewBundlePath];
		//		NSBundle *viewBundle = [NSBundle bundleWithPath:[cocoaViewBundlePath autorelease]];
		NSBundle *viewBundle = [NSBundle bundleWithPath:[cocoaViewBundlePath path]];
		if (viewBundle) {
			//			Class factoryClass = [viewBundle classNamed:(NSString *)factoryClassName];
			Class factoryClass = [viewBundle classNamed:factoryClassName];
			Boolean pluginClassIsValid;
			NSAssert(pluginClassIsValid = [[self class] pluginClassIsValid:factoryClass],
					 @"AU view's factory class does not properly implement the AUCocoaUIBase protocol");
			if (pluginClassIsValid) {
				//				id<AUCocoaUIBase> factoryInstance = [[[factoryClass alloc] init] autorelease];
				id factoryInstance = [[[factoryClass alloc] init] autorelease];
				NSAssert (factoryInstance != nil, @"Could not create an instance of the AU view factory");
				AUView = [factoryInstance uiViewForAudioUnit:unit 
													withSize:NSMakeSize(100, 100)];
				[AUView setPostsFrameChangedNotifications: YES];
				// Cocoa-View plug-in resizing
				// http://web.archiveorange.com/archive/v/q7bubwWB3B0VEDOeDDFG
				// There is a standard NSView notification that you can register for to 
				// receive these resize notifications.
				// NSViewFrameDidChangeNotification
				// https://developer.apple.com/library/mac/documentation/Cocoa/Reference/ApplicationKit/Classes/NSView_Class/Reference/NSView.html#//apple_ref/c/data/NSViewFrameDidChangeNotification
				[[NSNotificationCenter defaultCenter] 
				 addObserver:self
				 selector:@selector(audioUnitChangedViewSize:)
				 name:NSViewFrameDidChangeNotification
				 object:AUView];
			}
			// cleanup
			//			CFRelease(cocoaViewBundlePath);
			[cocoaViewBundlePath release];
			if (cocoaViewInfo) {
				for(unsigned int i = 0; i < numberOfClasses; i++) {
					CFRelease(cocoaViewInfo->mCocoaAUViewClass[i]);
				}
				//CFRelease(cocoaViewInfo->mCocoaAUViewBundleLocation);
				CFRelease(factoryClassName);
				free(cocoaViewInfo);
				cocoaViewInfo = NULL;
			}
		}
	}
	
	[self initWithAudioUnitCocoaView:AUView];
}


//--------------------------------------------------------------------------------------------------
//	initWithCocoaGenericViewForUnit:
//--------------------------------------------------------------------------------------------------
- (void)initWithCocoaGenericViewForUnit:(AudioUnit)unit
{
	
	NSLog(@"initWithGenericViewForUnit");
	// all Flags for displaying audio unit title, property, and parameter information in a generic view.
	// https://developer.apple.com/library/mac/documentation/MusicAudio/Reference/AUGenericView_ClassReference/Reference/Reference.html#//apple_ref/doc/uid/TP40009085-CH1-SW1
	UInt32 inFlags = AUViewTitleDisplayFlag | AUViewPropertiesDisplayFlag | AUViewParametersDisplayFlag;
	AUGenericView *AUView;
	do {
		AUView = [[AUGenericView alloc] initWithAudioUnit: unit displayFlags: inFlags];
		NSLog(@"initWithCocoaGenericViewForUnit: framesize %.2f@%.2f", AUView.frame.size.width, AUView.frame.size.height);
	} while (AUView.frame.size.width <= 0.0f);
	// avoid a threading problem in the view. 
	//http://lists.apple.com/archives/Coreaudio-api/2007/Jan/msg00173.html
	usleep(100000);
	[AUView setShowsExpertParameters:YES];
	[AUView autorelease];
	[self initWithAudioUnitCocoaView:AUView];
}

//--------------------------------------------------------------------------------------------------
//	initWithAudioUnitCocoaView:
//--------------------------------------------------------------------------------------------------
- (void)initWithAudioUnitCocoaView:(NSView *)audioUnitView
{
	NSLog(@"initWithAudioUnitCocoaView");
	_AUView = audioUnitView;
	NSLog(@"framesize %.2f@%.2f", audioUnitView.frame.size.width, audioUnitView.frame.size.height);
	NSRect contentRect = NSMakeRect(0, 0, audioUnitView.frame.size.width, audioUnitView.frame.size.height);
	NSWindow *window = [[[NSWindow alloc] initWithContentRect:contentRect
													styleMask:(NSTitledWindowMask |
															   NSClosableWindowMask |
															   NSMiniaturizableWindowMask |
															   NSResizableWindowMask)
													  backing:NSBackingStoreBuffered
														defer:NO]
						autorelease];
	if (window) {
		window.level = NSNormalWindowLevel;
		window.contentView = audioUnitView;
		[self setWindow: window];
		
		[[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(audioUnitChangedViewSize:) 
													  name:NSViewFrameDidChangeNotification 
													object:_AUView];
	}
}

//--------------------------------------------------------------------------------------------------
//	initWithAudioUnitCocoaView:
//
//	This technique embeds a carbon window inside of a cocoa
//	window wrapper, as described by Technical Note TN2213
//	("Audio Units: Embedding a Carbon View in a Cocoa Window")
//--------------------------------------------------------------------------------------------------
- (WindowRef)getCarbonWindowRef:(ComponentDescription *)desc audioUnit:(AudioUnit)unit size:(Float32Point *)size
{
	WindowRef carbonWindow;
	
	// Creating carbon view component
	Component comp = FindNextComponent(NULL, desc);
	verify_noerr_string(OpenAComponent(comp, &_carbonView), "opening carbon view component");
	if (comp == NULL) {
		return NULL;
	}
	// Creating a carbon window for the view
	Rect carbonWindowBounds = {100,100,300,300};
	verify_noerr_string(CreateNewWindow(kPlainWindowClass,
										(kWindowStandardHandlerAttribute |
										 kWindowCompositingAttribute),
										&carbonWindowBounds,
										&carbonWindow),
						"creating carbon window");
	
	// Creating carbon controls
	ControlRef rootControl;
	ControlRef viewPane;
	verify_noerr_string(GetRootControl(carbonWindow, &rootControl),
						"getting root control of carbon window");
	
	// Creating the view
	//Float32Point size = {0,0};
	Float32Point location = {0,0};
	verify_noerr_string(AudioUnitCarbonViewCreate(_carbonView,
												  unit,
												  carbonWindow,
												  rootControl,
												  &location,
												  size,
												  &viewPane),
						"creating carbon view for audio unit");
	
	// Putting everything in place
	GetControlBounds(viewPane, &carbonWindowBounds);
	size->x = carbonWindowBounds.right  - carbonWindowBounds.left;
	size->y = carbonWindowBounds.bottom - carbonWindowBounds.top;
	SizeWindow(carbonWindow, (short) (size->x + 0.5), (short) (size->y + 0.5), true);
	ShowWindow(carbonWindow);
	
	// Listening to window events
	EventTypeSpec windowEventTypes[] = {
		{kEventClassWindow, kEventWindowGetClickActivation},
		{kEventClassWindow, kEventWindowHandleDeactivate}
	};
	
	EventHandlerUPP ehUPP = NewEventHandlerUPP(CarbonWindowEventHandler);
	verify_noerr_string(InstallWindowEventHandler(carbonWindow,
												  ehUPP,
												  sizeof(windowEventTypes) / sizeof(EventTypeSpec),
												  windowEventTypes,
												  self,
												  &_carbonEventHandler),
						"setting up carbon window event handler");
	return carbonWindow;
}

//--------------------------------------------------------------------------------------------------
//	owningCocoaWindow:audioUnit:
//
//	This technique embeds a carbon window inside of a cocoa
//	window wrapper, as described by Technical Note TN2213
//	("Audio Units: Embedding a Carbon View in a Cocoa Window")
//--------------------------------------------------------------------------------------------------
- (void)owningCocoaWindow:(ComponentDescription)desc audioUnit:(AudioUnit)unit
{
	WindowRef carbonWindow;
	
	Float32Point size = {0,0};
	carbonWindow = [self getCarbonWindowRef:&desc audioUnit:unit size:&size];
	if (carbonWindow == NULL) {
		NSLog(@"carbon view not found.");
//		CFStringRef unitName = [self getAudioUnitName:unit];
//		const char *componentName = CFStringGetCStringPtr(unitName, encoding);
//		[unitName getCString:&componentName maxLength:[unitName length] encoding:encoding];
//		CFRelease(unitName);
		NSString *unitName = [self getAudioUnitName:unit];
		const char *componentName;
		componentName = [unitName cStringUsingEncoding:encoding];
		inform("carbon view not found.", componentName);
		free((void *) componentName);
		return;
	}
	_carbonWindow = carbonWindow;
	//	ShowWindow(_carbonWindow);
	NSWindow *wrapperWindow = [[[NSWindow alloc] initWithWindowRef:_carbonWindow] autorelease];
	
	// 	CGRect bounds = CGRectForWindow(_carbonWindow);
	// 	NSPoint frameOrigin;
	// 	frameOrigin.x = bounds.origin.x;
	// 	frameOrigin.y = bounds.origin.y;
	// 	[wrapperWindow setFrameOrigin:frameOrigin];
	
	NSWindow *owningCocoaWindow= [[[NSWindow alloc] initWithContentRect:NSMakeRect(0, 0, size.x + 1, size.y + 1)
															  styleMask:(NSClosableWindowMask | 
																		 NSMiniaturizableWindowMask |
																		 NSTitledWindowMask |
																		 NSResizableWindowMask)
																backing:NSBackingStoreBuffered
																  defer:NO]
								  autorelease];
	
	if (owningCocoaWindow) {
		[wrapperWindow setFrameOrigin:owningCocoaWindow.frame.origin];
		//		[owningCocoaWindow setFrameOrigin:wrapperWindow.frame.origin];
		[owningCocoaWindow addChildWindow:wrapperWindow ordered:NSWindowAbove];
		owningCocoaWindow.level = NSNormalWindowLevel;
		[self setWindow: owningCocoaWindow];
	}
}

//--------------------------------------------------------------------------------------------------
//	initWithGenericCarbonViewForUnit:
//--------------------------------------------------------------------------------------------------
- (void)initWithCarbonGenericViewForUnit:(AudioUnit)unit
{
	ComponentDescription d;
	
	// The four char-code subtype of a carbon-based view component
	// http://uri-labs.com/macosx_headers/AudioUnitCarbonView_h/index.html
	//	d.componentType = 0;
	d.componentType = kAudioUnitCarbonViewComponentType;
	d.componentSubType = kAUCarbonViewSubType_Generic;
	d.componentManufacturer = kAudioUnitManufacturer_Apple;
	d.componentFlags = 0;
	d.componentFlagsMask = 0;
	[self owningCocoaWindow:d audioUnit:unit];
}

//--------------------------------------------------------------------------------------------------
//	initWithCarbonViewForUnit:
//
//	This technique embeds a carbon window inside of a cocoa
//	window wrapper, as described by Technical Note TN2213
//	("Audio Units: Embedding a Carbon View in a Cocoa Window")
//--------------------------------------------------------------------------------------------------
- (void)initWithCarbonViewForUnit:(AudioUnit)unit
{
	NSLog(@"initWithCarbonViewForUnit");
	
	// Getting carbon view description
	UInt32 dataSize;
	Boolean isWriteable;
	
	verify_noerr_string(AudioUnitGetPropertyInfo(unit,
												 kAudioUnitProperty_GetUIComponentList,
												 kAudioUnitScope_Global,
												 0,
												 &dataSize,
												 &isWriteable),
						"getting size of carbon view info");
	
	ComponentDescription *desc = (ComponentDescription *)malloc(dataSize);
	
	verify_noerr_string(AudioUnitGetProperty(unit,
											 kAudioUnitProperty_GetUIComponentList,
											 kAudioUnitScope_Global,
											 0,
											 desc,
											 &dataSize),
						"getting carbon view info");
	
	ComponentDescription d = desc[0];
	[self owningCocoaWindow:d audioUnit:unit];
}

//**************************************************************************************************
//	audioUnitHasCocoaView:
//**************************************************************************************************
+ (BOOL)audioUnitHasCocoaView:(AudioUnit)unit
{
	UInt32 dataSize;
	UInt32 numberOfClasses;
	Boolean isWriteable;
	
	OSStatus result = AudioUnitGetPropertyInfo(unit,
											   kAudioUnitProperty_CocoaUI,
											   kAudioUnitScope_Global,
											   0,
											   &dataSize,
											   &isWriteable);
	
	numberOfClasses = (dataSize - sizeof(CFURLRef)) / sizeof(CFStringRef);
	
	return (result == noErr) && (numberOfClasses > 0);
}

- (BOOL)audioUnitHasCocoaView:(AudioUnit)unit
{
	return [[self class] audioUnitHasCocoaView:unit];
}

//**************************************************************************************************
//	audioUnitHasCarbonView:
//**************************************************************************************************
+ (BOOL)audioUnitHasCarbonView:(AudioUnit)unit
{
	UInt32 dataSize;
	Boolean isWriteable;
	OSStatus result = AudioUnitGetPropertyInfo(unit,
											   kAudioUnitProperty_GetUIComponentList,
											   kAudioUnitScope_Global,
											   0,
											   &dataSize, 
											   &isWriteable);
	
	return (result == noErr) && (dataSize >= sizeof(ComponentDescription));
}

- (BOOL)audioUnitHasCarbonView:(AudioUnit)unit
{
	return [[self class] audioUnitHasCarbonView:unit];
}

//--------------------------------------------------------------------------------------------------
//  editWindowClosed
//--------------------------------------------------------------------------------------------------
- (void)editWindowClosed
{
	extern void editWindowDealloced(AudioUnit audioUnit);
	
	// Any additional cocoa cleanup can be added here.
	editWindowDealloced([self audioUnit]);
	NSLog(@"now editWindowClosed...");
}

//--------------------------------------------------------------------------------------------------
//  close
//--------------------------------------------------------------------------------------------------
- (void)close
{
	NSLog(@"now close...");
	[[self window] orderOut: self];
	[super close];
}

+ (NSMutableArray *)subclasses:(Class)targetClass
{
	int classCount = objc_getClassList(NULL, 0);
	Class *classes = calloc(classCount, sizeof(Class));
	classCount = objc_getClassList(classes, classCount);
	Class* top = classes;
	
	NSMutableArray *subclasses = [[NSMutableArray array] autorelease];
	Class parentClass = targetClass;
	for (int i = 0; i < classCount; i++) {
		Class aClass = *top++;
		Class superclass = aClass;
		do {
			superclass = class_getSuperclass(superclass);
		} while (superclass && superclass != parentClass);
		if (superclass == nil) {
			continue;
		}
		[subclasses addObject:aClass];
	}
	free(classes);
	return subclasses;
}

+ (Class)detectClass:(Class)sourceClass useToolbar:(BOOL)useToolbar
{
	Class targetClass = self;
	if (useToolbar) {

#ifdef __x86_64__
		NSMutableArray *subclasses = [[NSMutableArray array] autorelease];
		AddSubclassesToArray(self, subclasses);
#elif __i386__
		NSMutableArray *subclasses = [self subclasses:self];
#endif //__x86_64__, __i386__

		for (Class aClass in subclasses) {
			if ([aClass instancesRespondToSelector:@selector(toolbar)]) {
				targetClass = aClass;
				break;
			}
		}
		[subclasses release];
	}
	return targetClass;
}

//**************************************************************************************************
//	showUI:useToolbar:
//
//	show edit window at center of display.
//**************************************************************************************************
+ (id)showUI:(AudioUnit)unit useToolbar:(BOOL)useToolbar
{
	id controller;
	
	// UIControllers
	if (PluginUIControllers == nil) {
		PluginUIControllers = [[NSMutableArray alloc] init];
	}
	for (unsigned int i = 0; i < [PluginUIControllers count]; i++) {
		controller = [PluginUIControllers objectAtIndex: i];
		if ([controller audioUnit] == unit) {
			[[controller window] makeKeyAndOrderFront: nil];
			return controller;
		}
	}
	
	Class class = [self detectClass:self useToolbar:useToolbar];
	controller = [[class alloc] initWithAudioUnit:unit];
	NSWindow *window = [controller window];
	NSRect screenFrame = [[window screen] frame];
	NSRect currentContentRect = [[controller window] contentRectForFrameRect:[window frame]];
	NSPoint origin;
	origin.x = (screenFrame.size.width - currentContentRect.size.width) / 2;
	origin.y = (screenFrame.size.height - currentContentRect.size.height) / 2;
	[window setFrameOrigin:origin];
	[window setTitle:[self getAudioUnitName:unit]];
	[window makeKeyAndOrderFront:nil];
	
	[PluginUIControllers addObject: controller];
	
	return controller;
}

//**************************************************************************************************
//	showUI:
//
//	show edit window at center of display.
//**************************************************************************************************
+ (id)showUI:(AudioUnit)unit;
{
	NSString *audioUnitName = [self getAudioUnitName:unit];
 	id controller = [self showUI:unit useToolbar:NO];
	[[controller window] setTitle:audioUnitName];
	//	CFRelease(name);
	return controller;
}

//--------------------------------------------------------------------------------------------------
//	CarbonWindowEventHandler
//
//	Technical Note TN2213 Audio Units: Embedding a Carbon View in a Cocoa Window
//	http://developer.apple.com/library/mac/technotes/tn2213/_index.html
//--------------------------------------------------------------------------------------------------
pascal OSStatus CarbonWindowEventHandler(EventHandlerCallRef nextHandlerRef,
										 EventRef event,
										 void *userData)
{
	PluginUIController *controller = (PluginUIController *)userData;
	UInt32 eventKind = GetEventKind(event);
	switch (eventKind) {
		case kEventWindowHandleDeactivate:
			ActivateWindow([controller carbonWindow], true);
			return noErr;
			
		case kEventWindowGetClickActivation:
			[[controller window] makeKeyAndOrderFront:nil];
			ClickActivationResult howToHandleClick = kActivateAndHandleClick;
			SetEventParameter(event, 
							  kEventParamClickActivation,
							  typeClickActivationResult,
							  sizeof(ClickActivationResult),
							  &howToHandleClick);
			return noErr;
	}
	
	return eventNotHandledErr;
}

static CGRect CGRectForWindow(WindowRef window) {
	CGRect bounds = { { 0, 0 }, { 0, 0 } };
	HIWindowGetBounds(window, kWindowContentRgn, kHICoordSpace72DPIGlobal,
					  &bounds);
	return bounds;
}

// delegates
#pragma mark -
#pragma mark NSWindowDelegate

//- (BOOL)windowShouldClose:(id)sender {return [[self window] windowShouldClose:sender];}
//- (id)windowWillReturnFieldEditor:(NSWindow *)sender toObject:(id)client
//{
//	return [[self window] windowWillReturnFieldEditor:sender toObject:client];
//}

//--------------------------------------------------------------------------------------------------
//    windowWillResize:toSize:
//--------------------------------------------------------------------------------------------------
- (NSSize)windowWillResize:(NSWindow *)sender toSize:(NSSize)frameSize
{
	// at first, minimumWindowSize = audiounit view size
	NSSize minimumWindowSize = [self audioUnitViewSize];
	NSSize windowFrameSize = [sender frame].size;
	NSSize contentViewFrameSize = [[sender contentView] frame].size;
	NSSize delta;
	
	// minimumWindowSize = audiounit view size + delta
	delta.width = windowFrameSize.width - contentViewFrameSize.width;
	delta.height = windowFrameSize.height - contentViewFrameSize.height;
	minimumWindowSize.width += delta.width;
	minimumWindowSize.height += delta.height;
	NSSize preferredSize = frameSize;
	if (preferredSize.height < minimumWindowSize.height) {
		preferredSize.height = minimumWindowSize.height;
	}
	if (preferredSize.width < minimumWindowSize.width) {
		preferredSize.width = minimumWindowSize.width;
	}
	return preferredSize;
}

//- (NSRect)windowWillUseStandardFrame:(NSWindow *)window defaultFrame:(NSRect)newFrame {}
//- (BOOL)windowShouldZoom:(NSWindow *)window toFrame:(NSRect)newFrame {}
//- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window {}
//#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_3
//- (NSRect)window:(NSWindow *)window willPositionSheet:(NSWindow *)sheet usingRect:(NSRect)rect {}
//#endif

//#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_5
///* 
// If a window has a representedURL, 
// the window will by default show a path popup menu for a command-click on 
// a rectangle containing the window document icon button and the window title.  
// The window delegate may implement -window:shouldPopupDocumentPathMenu: 
// to override NSWindow's default behavior for path popup menu.  
// A return of NO will prevent the menu from being shown.  
// A return of YES will cause the window to show the menu passed to this method, 
// which by default will contain a menuItem for each path component of the representedURL.  
// If the representedURL has no path components, the menu will have no menu items.  
// Before returning YES, the window delegate may customize the menu by changing the menuItems.  
// menuItems may be added or deleted, and each menuItem title, action, or target may be modified. 
// */
//- (BOOL)window:(NSWindow *)window shouldPopUpDocumentPathMenu:(NSMenu *)menu
//{
//	return [[self window] shouldPopUpDocumentPathMenu:menu];
//}
//
///* The window delegate may implement 
// -window:shouldDragDocumentWithEvent:from:withPasteboard: 
// to override NSWindow document icon's default drag behavior.  
// The delegate can prohibit the drag by returning NO.  
// Before returning NO, the delegate may implement its own dragging behavior using 
// -[NSWindow dragImage:at:offset:event:pasteboard:source:slideBack:].  
// Alternatively, the delegate can enable a drag by returning YES, 
// for example to override NSWindow's default behavior of 
// prohibiting the drag of an edited document.  
// Lastly, the delegate can customize the pasteboard contents before returning YES.
// */
//- (BOOL)window:(NSWindow *)window 
//shouldDragDocumentWithEvent:(NSEvent *)event 
//		  from:(NSPoint)dragImageLocation 
//withPasteboard:(NSPasteboard *)pasteboard
//{
//	return [[self window] window:window
//	 shouldDragDocumentWithEvent:event 
//							from:dragImageLocation 
//				  withPasteboard:pasteboard];
//}	

/*  Notifications */

#pragma mark NSWindowDelegate Notifications

//- (void)windowDidResize:(NSNotification *)notification {}
//- (void)windowDidExpose:(NSNotification *)notification {}
//- (void)windowWillMove:(NSNotification *)notification {}
//- (void)windowDidMove:(NSNotification *)notification {}
//- (void)windowDidBecomeKey:(NSNotification *)notification {}
//- (void)windowDidResignKey:(NSNotification *)notification {}
//- (void)windowDidBecomeMain:(NSNotification *)notification {}
//- (void)windowDidResignMain:(NSNotification *)notification {}

//--------------------------------------------------------------------------------------------------
//	windowWillClose
//	now window is closing...
//--------------------------------------------------------------------------------------------------
- (void)windowWillClose:(NSNotification *)notification
{
	//https://developer.apple.com/jp/documentation/Cocoa/Conceptual/AppArchitecture/Tasks/GracefulAppTermination.html
	NSLog(@"windowWillClose");
	[[NSNotificationCenter defaultCenter]removeObserver:self 
												   name:NSViewFrameDidChangeNotification 
												 object:_AUView];
	[[self window] setDelegate:nil];
	[self editWindowClosed];
	[self setWindow:nil];
	[self setAudioUnit: nil];
	[PluginUIControllers removeObject: self];
	NSLog(@"here1");
	[self release];
	NSLog(@"here2");
}

//- (void)windowWillMiniaturize:(NSNotification *)notification {}
//- (void)windowDidMiniaturize:(NSNotification *)notification {}
//- (void)windowDidDeminiaturize:(NSNotification *)notification {}
//- (void)windowDidUpdate:(NSNotification *)notification {}
//- (void)windowDidChangeScreen:(NSNotification *)notification {}
//#if MAC_OS_X_VERSION_MAX_ALLOWED >= MAC_OS_X_VERSION_10_4
//- (void)windowDidChangeScreenProfile:(NSNotification *)notification {}
//#endif
//- (void)windowWillBeginSheet:(NSNotification *)notification {}
//- (void)windowDidEndSheet:(NSNotification *)notification {}
//#endif
//- (void)windowWillStartLiveResize:(NSNotification *)notification {}    AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;
//- (void)windowDidEndLiveResize:(NSNotification *)notification {}   AVAILABLE_MAC_OS_X_VERSION_10_6_AND_LATER;

// notification
#pragma mark -
#pragma mark notification
//--------------------------------------------------------------------------------------------------
//	audioUnitChangedViewSize:
//
//	sent by window resizing.
//--------------------------------------------------------------------------------------------------
- (void)audioUnitChangedViewSize:(NSNotification *)notification
{
	[[NSNotificationCenter defaultCenter] removeObserver:self 
													name:NSViewFrameDidChangeNotification
												  object:_AUView];
	// restore current window size
	NSSize currentWindowSize = [[self window] frame].size;
	
	// resizing
	NSWindow *window = [self window];
	NSRect newRect = window.frame;
	NSSize newSize = [window frameRectForContentRect:((NSView *)[notification object]).frame].size;
	newRect.origin.y -= newSize.height - newRect.size.height;
	newRect.size = newSize;
	[window setFrame:newRect display:YES];
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(audioUnitChangedViewSize:) 
												 name:NSViewFrameDidChangeNotification
											   object:_AUView];
	// update _audioUnitViewSize
	NSSize delta;
	delta.width = newSize.width - currentWindowSize.width;
	delta.height = newSize.height - currentWindowSize.height;
	_audioUnitViewSize.width += delta.width;
	_audioUnitViewSize.height += delta.height;
}

@end

#pragma mark -
#pragma mark c function prototypes
/*----------------------------------------------------------------------------
 c function prototypes
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------
 countOfUIControllers
 ----------------------------------------------=------------*/
int countOfUIControllers(void) {
	return [PluginUIControllers count];
}

/*-----------------------------------------------------------
 closeUIControllerAt
 ----------------------------------------------=------------*/
int closeUIControllerAt(int controllerIndex) {
	NSLog(@"closeUIControllerAt");
	int count = countOfUIControllers();
	if (controllerIndex < 0 || controllerIndex > count - 1) {
		return NO;
	}
	id controller = [PluginUIControllers objectAtIndex: controllerIndex];
	[controller close];
	return YES;
}

/*-----------------------------------------------------------
 closeAllUIControllers
 ----------------------------------------------=------------*/
void closeAllUIControllers(void) {
	NSLog(@"closeAllUIControllers");
	int count = countOfUIControllers();
	for (int i = count - 1; i > 0; i--) {
		closeUIControllerAt(i);
	}
}


#endif //TARGET_OS_IPHONE
