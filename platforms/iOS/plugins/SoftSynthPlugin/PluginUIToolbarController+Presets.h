//
//  PluginUIToolbarController+Presets.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/16.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "PluginUIToolbarController.h"

@interface PluginUIToolbarController (Presets)

- (AUPreset)getCurrentPreset:(AudioUnit)unit;
+ (AUPreset)getCurrentPreset:(AudioUnit)unit;

+ (NSString *)getCurrentPresetName:(AudioUnit)unit;
- (NSString *)getCurrentPresetName:(AudioUnit)unit;
+ (void)notifyAUParameterListenerOfParameterChanges: (AudioUnit)unit;
- (void)notifyAUParameterListenerOfParameterChanges: (AudioUnit)unit;

+ (CFArrayRef)getPresets:(AudioUnit)unit;
+ (OSStatus)setPreset:(AudioUnit)unit to:(UInt32) presetNumber;
+ (OSStatus)setPreset:(AudioUnit)unit named:(CFStringRef)presetName;
+ (OSStatus)setPreset:(AudioUnit)unit presetName:(CFStringRef)presetName presetNumber:(UInt32) presetNumber;

@end
