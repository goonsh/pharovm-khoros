//
//  PluginUIController+AudioUnit.h
//  SoftSynthPlugin
//
//  Created by NISHIHARA Satoshi on 13/09/16.
//  Copyright 2013 goonsh@gmail.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "PluginUIController.h"

@interface PluginUIController (AudioUnit)

+ (CFStringRef)getAudioComponentName:(AudioComponent)component;
+ (NSString *)getAudioUnitName:(AudioUnit)unit;
+ (NSString *)getAudioUnitBasenameByIndex:(AudioUnit)unit;
+ (NSString *)getAudioUnitBasename:(AudioUnit)unit;
+ (NSArray *)getManufacturerAndNameFromAudioUnit:(AudioUnit)unit;

- (NSString *)getAudioUnitName:(AudioUnit)unit;
- (CFStringRef)getAudioComponentName:(AudioComponent)component;
- (NSString *)getAudioUnitBasename:(AudioUnit)unit;
- (NSArray *)getManufacturerAndNameFromAudioUnit:(AudioUnit)unit;

@end
