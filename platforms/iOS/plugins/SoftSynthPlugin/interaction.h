/*
 *  interaction.h
 *  SoftSynthPlugin
 *
 *  Created by NISHIHARA Satoshi on 13/08/24.
 *  Copyright 2013 goonsh@gmail.com. All rights reserved.
 *
 */

int inform(const char *messageText, const char *informativeText);
int confirm(const char *messageText, const char *informativeText);
int confirmOrOther(const char *cmessageText, const char *cinformativeText, Boolean useCancel, Boolean useOther, const char *otherButtonName);
